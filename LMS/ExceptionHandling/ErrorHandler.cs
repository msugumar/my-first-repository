﻿using System;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Web.UI;
using LMSDAL;


namespace ExceptionalHandling
{
    public  class ErrorHandler
    {
       
        public  LMSDAL.ExceptionDAL objDAL;
        const string EVENT_LOG_SOURCE = "LMS";
        
        public int RaiseError(Exception exceptionFromPage, string strPath)
        {
            string strSource = "";
            string strMessage = "";
            string strStackTrace = "";
            string strInnerException = "";
            strSource = "" + exceptionFromPage.Source;
            strMessage = "" + exceptionFromPage.Message;
            strStackTrace = "" + exceptionFromPage.StackTrace;
            if (exceptionFromPage.InnerException != null)
                strInnerException = "" + exceptionFromPage.InnerException.ToString();
            else
                strInnerException = string.Empty;
            
           
            objDAL = new LMSDAL.ExceptionDAL();
           // objDAL.WriteExceptionInfo(strSource,strMessage,strStackTrace,strInnerException);
            int ErrorLogNumber =objDAL.WriteExceptionInfo(strSource, strMessage, strStackTrace, strInnerException);
            return ErrorLogNumber;
            
            //objClass.ErrorLogNumber(intReturnValue);
            throw new Exception("LMSException", exceptionFromPage);
            // int ErrorLogNumber=objDAL
          
        }
    }
}



//Error Handler class ends
