﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
    public abstract class IssueAbs
    {
   
        public abstract DataSet GetIssueInfo();
        //public abstract DataSet GetFilterIssueInfo(IssueInfo obj);
        public abstract DataSet GetItemTypeInfo();      
        public abstract DataSet GetUserInfo();
        public abstract DataSet GetFilterItemInfo(IssueInfo obj);
        public abstract int SaveIssueInfo(IssueInfo obj);
        public abstract int SaveRenewalInfo(IssueInfo obj);
        public abstract DataSet GetFilterRenewalInfo(IssueInfo obj);
        public abstract string[] Accessverifyer(IssueInfo obj);
    }
   }
