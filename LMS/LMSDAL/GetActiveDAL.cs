﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;
namespace LMSDAL
{
    public class GetActiveDAL:GetActiveAbs
    {
        System.Data.Common.DbCommand dbCom;
        public override DataSet GetActiveDirectory()
        {

            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UspGetActiveDirectory");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {

                throw ex;


            }
        } //GetCategor
    }
}
