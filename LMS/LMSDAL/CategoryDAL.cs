﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class CategoryDAL : TypeCategoryAbs
    {
        System.Data.Common.DbCommand dbCom;

        public override DataSet GetCategoryDetails()
        {

            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspCategory");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {

                throw ex;


            }
        } //GetCategoryInfo ends
        public override DataSet GetStatusDescriptionInfo()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetStatusDetail");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {

                throw ex;


            }
        }
        public override string[] Accessverifyer(CategoryInfo obj)
        {
            try
            {
                string[] strResult = new string[2];
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
                //DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitApproveAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
                DataAccess.DataAccess.GetScalar(dbCom);

                strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
                strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
                //strResult[2] = (dbCom.Parameters["@intUserId"].Value).ToString();
                //strResult[3] = (dbCom.Parameters["@vchEmailId"].Value).ToString();
                ////strResult[4] = (dbCom.Parameters["@bitApproveAccess"].Value).ToString();
                //strResult[4] = (dbCom.Parameters["@vchUserName"].Value).ToString();
                return strResult;
            }
            catch (Exception ex)
            {

                throw ex;


            }

        }
        public override int SaveCategoryType(CategoryInfo obj)
            {
                Int32 intReturnValue = default(Int32);
                try
                {
                    dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspSaveCategory");
                    DataAccess.DataAccess.db.AddInParameter(dbCom, "@CategoryId", DbType.Int32, obj.CategoryId);
                    DataAccess.DataAccess.db.AddInParameter(dbCom, "@Category", DbType.String, obj.Category);
                    DataAccess.DataAccess.db.AddInParameter(dbCom, "@CategoryDescription", DbType.String, obj.CategoryDescription);
                    DataAccess.DataAccess.db.AddInParameter(dbCom, "@StatusID", DbType.Int32, obj.StatusID);
                    DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 0);
                    intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
                    //return intReturnValue;
                }
                catch (Exception ex)
                {

                    throw ex;

                }
                return intReturnValue;
            } //InsertItemType ends
        }

    }
