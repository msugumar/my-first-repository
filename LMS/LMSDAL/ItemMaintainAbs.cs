﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using LMSBLL;


namespace LMSDAL
{
    public abstract class ItemMaintainAbs
    {
       
        public abstract DataSet GetItemMaintainInfo();
        public abstract DataSet GetItemTypeDetails();
        public abstract DataSet GetItemStatusDetails();
        public abstract DataSet GetItemDepartmentDetails();
        public abstract DataSet GetItemCategoryDetails();
        public abstract int InsertItemDetails(ItemMaintainInfo obj);
        ////public abstract string[] ValidateUser(ItemMaintainInfo obj);
        //public abstract string[] UserChecker(ItemMaintainInfo obj);
        public abstract string[] Accessverifyer(ItemMaintainInfo obj);
        public abstract DataSet GetItemTypeDetails123();
   
    
        
    }
}
