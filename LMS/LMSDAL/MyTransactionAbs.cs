﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
   public abstract class MyTransactionAbs
    {
       public abstract DataSet GetMyTransactionInfo(MyTransactionInfo obj);
       public abstract DataSet ViewMyTransactionInfo(MyTransactionInfo obj);
       public abstract DataSet MyTransactionItemStatus(MyTransactionInfo obj);
        public abstract string[] Accessverifyer(MyTransactionInfo obj);
       // public abstract DataSet MyTransactionDate();
        
    }
}
