﻿using System;
using System.Collections;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class RoleDal : RoleAbs
    {
       // int intReturnValue;
        System.Data.Common.DbCommand dbCom;
        #region "Populate Roles in Grid"

        public override DataSet PopulateRoleGrid()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGvRoleDetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Populate Roles To DropDown"

        public override DataSet PopulateRoles()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetRoles");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Populate Status....."

        public override DataSet PopulateStatus()
        {
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetFunctionalStatus");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region "Save Roles....."

        public override int SaveRoles(RolesInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspSaveMasterRoles_sk1");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@RoleName", DbType.String, obj.rolename);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@StatusId", DbType.Int32, obj.statusinfo);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 0);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return intReturnValue;
        }

        #endregion

        #region "Save Role Assignment....."

        public override int SaveRoleAssignment(RolesInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspFormsAssign_trace_sk");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@RoleId", DbType.Int32, obj.roleid);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormID", DbType.String, obj.FormId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchWriteAccess", DbType.String, obj.bitWriteAccess);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchReadAccess", DbType.String, obj.bitReadAccess);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return intReturnValue;
        }

        #endregion

        #region "Populate Form Details"
        public override DataSet PopulateForms()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspPopulateFormDetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Retrive Values For Form Page"

        public override DataSet RetriveValues(RolesInfo obj)
        {
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspPopulateFormRetrival");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@RoleId", DbType.Int32, obj.roleid);

                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Retrive Role Page Status"
        public override DataSet RetriveRoleStatus(RolesInfo obj)
        {
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspPopulateRolesStatus");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@RoleId", DbType.Int32,obj.roleid);

                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region "Populate Roles to Form Page Drop Down"
        //public override DataSet populateFormRoles()
        //{
        //    try
        //    {

        //        dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetFormRoles");
        //        return DataAccess.DataAccess.GetDataSet(dbCom);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //#endregion
#endregion

        public override string[] Accessverifyer(RolesInfo obj)
        {
            try
            {
                string[] strResult = new string[2];
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
                //DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitApproveAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
                DataAccess.DataAccess.GetScalar(dbCom);

                strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
                strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
                //strResult[2] = (dbCom.Parameters["@intUserId"].Value).ToString();
                //strResult[3] = (dbCom.Parameters["@vchEmailId"].Value).ToString();
                //strResult[4] = (dbCom.Parameters["@bitApproveAccess"].Value).ToString();
                //strResult[4] = (dbCom.Parameters["@vchUserName"].Value).ToString();
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}


