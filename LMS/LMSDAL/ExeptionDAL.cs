﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Data;
using System.Data.SqlClient;
using LMSBLL;

namespace LMSDAL
{
    public class ExceptionDAL : ExceptionAbs
    {
        int intReturnValue;
        System.Data.Common.DbCommand dbCom;
        public override int GetExceptionInfo(ExceptionInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("USPEXCEPTIONHANDLING");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRSOURCE", DbType.String, obj.ExpSource);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRMESSAGE", DbType.String, obj.ExpMessage);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRSTACKTRACE", DbType.String, obj.ExpStackTrace);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRINNEREXCEPTION", DbType.String, obj.ExpInnerException);
           // return DataAccess.DataAccess.GetDataSet(dbCom);
            intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
            return intReturnValue;
        }
    }
}

