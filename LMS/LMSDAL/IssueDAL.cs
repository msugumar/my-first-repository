﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;


namespace LMSDAL
{
    public class IssueDAL : IssueAbs
    {
        //int intRetrunValue;
        System.Data.Common.DbCommand dbCom;

        #region "Populate Transaction"
        public override DataSet GetIssueInfo()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetTransaction");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //throw new NotImplementedException();
        }
        #endregion

        #region "Populate ItemType"
        public override DataSet GetItemTypeInfo()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetItemTypeDetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //throw new NotImplementedException();
        }  
        #endregion

        #region "Populate User"
        public override DataSet GetUserInfo()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetUserDetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //throw new NotImplementedException();
        }
        #endregion

        #region "Populate Item"
        public override DataSet GetFilterItemInfo(IssueInfo obj)
        {
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetFiterItemDetails");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemTypeId", DbType.Int32, obj.ItemTypeId);
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //throw new NotImplementedException();
        }
        #endregion

        #region "Save Transaction"
        public override int SaveIssueInfo(IssueInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspSaveTransactiondetails");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@TransactionId", DbType.Int32, obj.TransactionId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemId", DbType.Int32, obj.ItemId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@UserId", DbType.Int32, obj.UserId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@IssueDate", DbType.DateTime, obj.IssueDate);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@DueDate", DbType.DateTime, obj.DueDate);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@ReturnDate", DbType.DateTime, obj.ReturnDate);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@StatusId", DbType.Int32, obj.StatusId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@ReturnedFlag", DbType.Int32, obj.ReturnedFlag);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 1);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return intReturnValue;
        }
        #endregion

        #region "Populate Renewal"
        public override DataSet GetFilterRenewalInfo(IssueInfo obj)
        {
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetRenewalDetails");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@TransactionId", DbType.Int32, obj.TransactionId);
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //throw new NotImplementedException();
        }
        #endregion

        public override string[] Accessverifyer(IssueInfo obj)
        {
            try
            {

                string[] strResult = new string[2];
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
                //DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitApproveAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
                DataAccess.DataAccess.GetScalar(dbCom);

                strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
                strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
                //strResult[2] = (dbCom.Parameters["@intUserId"].Value).ToString();
                //strResult[3] = (dbCom.Parameters["@vchEmailId"].Value).ToString();
                ////strResult[4] = (dbCom.Parameters["@bitApproveAccess"].Value).ToString();
                //strResult[4] = (dbCom.Parameters["@vchUserName"].Value).ToString();
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
                


        }
        
        #region "Save Renewal"
        public override int SaveRenewalInfo(IssueInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspSaveRenewaldetails");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@RenewalId", DbType.Int32, obj.RenewalId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@TransactionId", DbType.Int32, obj.TransactionId);
                //DataAccess.DataAccess.db.AddInParameter(dbCom, "@SerialNumber", DbType.Int32, obj.SerialNumber);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@RenewalDate",DbType.DateTime,obj.RenewalDate);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 1);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return intReturnValue;
        }
        #endregion

        //public override DataSet GetFilterIssueInfo(IssueInfo obj)
        //{
        //    try
        //    {

        //        dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetTransactionSample");
        //        DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemTypeId", DbType.Int32, obj.ItemTypeId);
        //        return DataAccess.DataAccess.GetDataSet(dbCom);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    //throw new NotImplementedException();
        //}
    }
}
