﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class SearchDAL : SearchAbs
    {
        System.Data.Common.DbCommand dbCom;
        
        public override DataSet GetSearchInfo()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspItemMasterDetailsSearch");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetTitleInfo()
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetTitle");
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet  GetCategoryInfo()
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetCategory");
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetItemTypeInfo()
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetItemType");
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetAuthorInfo()
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetAuthor");
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetFiterByItemTypeInfo(SearchInfo obj)
        {
            try
            {

            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetFiterItemTypeDetails");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemTypeId", DbType.Int32, obj.SearchItemID);
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //throw new NotImplementedException();
        }

        public override DataSet GetFiterByTitleInfo(SearchInfo obj)
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetFiterTitleDetails");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemId", DbType.String, obj.SearchTitle);
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetFiterByCategoryInfo(SearchInfo obj)
        {
            try
            {

            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetFiterCategoryDetails");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@CategoryId", DbType.Int32, obj.SearchCategoryID);
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetFiterByAuthorInfo(SearchInfo obj)
        {
            try
            {

            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetFiterAuthorDetails");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemId", DbType.String, obj.SearchAuthor);
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet PutSearchDetail(SearchInfo obj)
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspSearchDetail");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@ITEMTYPEID", DbType.Int32, obj.SearchItemID);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@CATEGORYID", DbType.Int32, obj.SearchCategoryID);
            //DataAccess.DataAccess.db.AddInParameter(dbCom, "@ITEM_TYPE", DbType.String, obj.SearchItemType);
            //DataAccess.DataAccess.db.AddInParameter(dbCom, "@CATEGORY", DbType.String, obj.SearchCategory);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@TITLE", DbType.String, obj.SearchTitle);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@AUTHOR", DbType.String, obj.SearchAuthor);
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string[] Accessverifyer(SearchInfo obj)
        {
            try
            {
            string[] strResult = new string[2];
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
            DataAccess.DataAccess.GetScalar(dbCom);
            strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
            strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
            return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     }
}
