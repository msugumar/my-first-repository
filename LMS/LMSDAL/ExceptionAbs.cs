﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using LMSBLL;

namespace LMSDAL
{
    public abstract class ExceptionAbs
    {
        public abstract Int32 WriteExceptionInfo(String strSource,String strMessage,String strStackTrace,String strInnerException);
    }
}
