﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
    public abstract class ItemTypeAbs
    {
        public abstract DataSet GetItemTypeInfo();
        public abstract DataSet GetStatusDescriptionInfo();
        public abstract int InsertItemType(ItemTypeInfo obj);
        public abstract string[] Accessverifyer(ItemTypeInfo obj);
    }
}
