﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
   public abstract class StatusAbs    
   {
          
       public abstract DataSet GetStatusInfo();
       //public abstract DataSet GetDropDownValue();
       public abstract int InsertStatus(StatusInfo obj);
       public abstract string[] Accessverifyer(StatusInfo obj);
      // public abstract int GetStatus(StatusInfo obj);
   
   
      
       
    }
}
