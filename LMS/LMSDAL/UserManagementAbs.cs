﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
    public abstract class UserManagementAbs
    {
        public abstract DataSet GetUserNameInfo();
        public abstract DataSet GetUserRoleInfo();
        public abstract DataSet GetUserEmailandLoginInfo(UserManagementInfo obj);
        public abstract int InsertUserRole(UserManagementInfo obj);
        public abstract string[] Accessverifyer(UserManagementInfo obj);
    }
}
