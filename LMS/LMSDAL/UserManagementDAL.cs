﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class UserManagementDAL : UserManagementAbs
    {
        System.Data.Common.DbCommand dbCom;

        public override DataSet GetUserNameInfo()
        { 
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetUserName");
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetUserRoleInfo()
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetUserRole");
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override DataSet GetUserEmailandLoginInfo(UserManagementInfo obj)
        {
            try
            {
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspUserMagtEmailandLoginDisplayAndRole");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@USERID", DbType.Int32, obj.UserID);
            return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override int InsertUserRole(UserManagementInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspSaveUserRole");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@USERID", DbType.Int32, obj.UserID);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@ROLEID", DbType.Int32, obj.RoleID);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 0);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
                return intReturnValue;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            //return intReturnValue;
        }

        public override string[] Accessverifyer(UserManagementInfo obj)
        {
            try
            {
            string[] strResult = new string[2];
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
            DataAccess.DataAccess.GetScalar(dbCom);
            strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
            strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
            return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
