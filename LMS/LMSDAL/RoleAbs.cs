﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
    #region "Abstract Class"
    public abstract class RoleAbs
    {
       public abstract DataSet PopulateRoleGrid();
       public abstract DataSet PopulateRoles();
       public abstract DataSet RetriveRoleStatus(RolesInfo obj);
       public abstract DataSet PopulateStatus();
       public abstract int SaveRoles(RolesInfo obj);
       public abstract DataSet PopulateForms();
       public abstract int SaveRoleAssignment(RolesInfo obj);
       public abstract DataSet RetriveValues(RolesInfo obj);
       public abstract string[] Accessverifyer(RolesInfo obj);
    }
    #endregion
}
