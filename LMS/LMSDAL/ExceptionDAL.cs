﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace LMSDAL
{
    public class ExceptionDAL : ExceptionAbs
    {
        System.Data.Common.DbCommand dbCom;


        public override Int32 WriteExceptionInfo(String strSource, String strMessage, String strStackTrace, String strInnerException)
        {

             Int32 intReturnValue = default(Int32);
             dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspExceptionHandling");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRSOURCE", DbType.String, strSource);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRMESSAGE", DbType.String, strMessage);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRSTACKTRACE", DbType.String, strStackTrace);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@STRINNEREXCEPTION", DbType.String, strInnerException);

                //return DataAccess.DataAccess.GetDataSet(dbCom);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
                //intReturnValue = 1;
                return intReturnValue;
          
    }
    }
}
