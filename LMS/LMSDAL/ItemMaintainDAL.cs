﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class ItemMaintainDal : ItemMaintainAbs
    {
        //int intRetrunValue;
        System.Data.Common.DbCommand dbCom;

        public override DataSet GetItemMaintainInfo()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspDispItemDetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch(Exception ex)
            {
                throw ex;
            }


            //throw new NotImplementedException();
        }

        public override DataSet GetItemTypeDetails()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetItemMasterItemtypes");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public override DataSet GetItemTypeDetails123()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspGetItemMasterItemtypes");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public override DataSet GetItemStatusDetails()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspItemstatusdetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public override DataSet GetItemCategoryDetails()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspItemsmastercategorydetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public override DataSet GetItemDepartmentDetails()
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspItemsmasterdeptdetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        

        public override int InsertItemDetails(ItemMaintainInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspItemMasterDetailssaved");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@itemId", DbType.Int32, obj.itemId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemTypeId", DbType.Int32, obj.ItemTypeId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@title", DbType.String, obj.title);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@CategoryId", DbType.Int32, obj.category);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@DeptId", DbType.Int32, obj.DeptId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@price", DbType.Int32, obj.price);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Author", DbType.String, obj.author);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@StatusId", DbType.Int32, obj.StatusId);                
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 0);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
                return intReturnValue;
            }
            catch (Exception ex)
            {
                throw ex;
                //ErrorHandler.RaiseError(ex, strPath);
            }
            //dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("usp_ItemDetailCreationMaster_save");
            //return DataAccess.DataAccess.GetDataSet(dbCom);
            //return intReturnValue;
        }
        public override string[] Accessverifyer(ItemMaintainInfo obj)
        {
            try
            {
                string[] strResult = new string[2];
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
                //DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitApproveAccess", DbType.Boolean, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
                DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
                DataAccess.DataAccess.GetScalar(dbCom);

                strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
                strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
                //strResult[2] = (dbCom.Parameters["@intUserId"].Value).ToString();
                //strResult[3] = (dbCom.Parameters["@vchEmailId"].Value).ToString();
                ////strResult[4] = (dbCom.Parameters["@bitApproveAccess"].Value).ToString();
                //strResult[4] = (dbCom.Parameters["@vchUserName"].Value).ToString();
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        //public override string[] UserChecker(ItemMaintainDal obj)
        //{
        //    //try
        //    //{
               
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    //ErrorHandler.RaiseError(ex, strPath);
        //    //}
           
        //}



        //public override int InsertCategory(ItemMaintainInfo obj)
        //{
           
        //} //InsertCategory ends

        //public object GetCategoryInfo()
        //{
        //    throw new NotImplementedException();
        //}

        
    }
}
