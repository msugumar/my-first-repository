﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Text;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class StatusDAL : StatusAbs
    {
        //int intReturnValue;
        System.Data.Common.DbCommand dbCom;

        public override DataSet GetStatusInfo()
        {

            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspgetstatusDetails");
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
               
            }
            
        }

        public override string[] Accessverifyer(StatusInfo obj)
        {
            string[] strResult = new string[2];
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
            //DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitApproveAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
            DataAccess.DataAccess.GetScalar(dbCom);

            strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
            strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
            //strResult[2] = (dbCom.Parameters["@intUserId"].Value).ToString();
            //strResult[3] = (dbCom.Parameters["@vchEmailId"].Value).ToString();
            ////strResult[4] = (dbCom.Parameters["@bitApproveAccess"].Value).ToString();
            //strResult[4] = (dbCom.Parameters["@vchUserName"].Value).ToString();
            return strResult;


        }//GetStatusInfo ends




        //public override DataSet GetDropDownValue()
        //{
        //    dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspgetstatus_ref_id");
        //    return DataAccess.DataAccess.GetDataSet(dbCom);
        //}
        //public override int GetStatus(StatusInfo obj)
        //{
        //    Int32 intReturnValue = default(Int32);
        //    dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("uspgetstatusDetails_Trace ");
        //    DataAccess.DataAccess.db.AddInParameter(dbCom, "@VCH_STATUS_TEXT", DbType.String, obj.StatusText);
        //    DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 0);
        //    intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
        //    return intReturnValue;
        //}

        public override int InsertStatus(StatusInfo obj)
        {
            Int32 intReturnValue = default(Int32);
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("USPSAVESTATUSDETAILS");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@nvchStatus", DbType.String, obj.status);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@intStatusId", DbType.String, obj.StatusId);
                //DataAccess.DataAccess.db.AddInParameter(dbCom, "@VCH_STATUS_DESCRIPTION", DbType.String, obj.StatusDescription);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Output", DbType.Int32, 0);
                intReturnValue = Convert.ToInt32(DataAccess.DataAccess.GetScalar(dbCom));
                //return intReturnValue;
            }
            catch (Exception ex)
            {
                throw ex;
                
            }

            return intReturnValue;
         
        }
        //InsertCategory ends



    }
    }

