﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class MyTransactionDAL : MyTransactionAbs
    {
        
        System.Data.Common.DbCommand dbCom;

        #region "sp populate page load "
        public override DataSet GetMyTransactionInfo(MyTransactionInfo obj)
        {
            try
            {
                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("USPGetTransactionDetails");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@strLoginId", DbType.String, obj.LoginId);
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
           
        } 
#endregion 

        #region "View My Transaction"
        public override DataSet ViewMyTransactionInfo(MyTransactionInfo obj)
        {
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("USPViewMyTransaction");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Startdate", DbType.DateTime, obj.StartDate);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@Enddate", DbType.DateTime, obj.EndDate);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@ItemStatus", DbType.String, obj.ItemStatus);
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@strLoginId", DbType.String, obj.LoginId);
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion 

        #region "sp to get status in dropdown"
        public override DataSet MyTransactionItemStatus(MyTransactionInfo obj)
        {
            try
            {

                dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("USPMyTransactionItemStatus_Trace");
                DataAccess.DataAccess.db.AddInParameter(dbCom, "@strLoginId", DbType.String, obj.LoginId);
                return DataAccess.DataAccess.GetDataSet(dbCom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion 

        public override string[] Accessverifyer(MyTransactionInfo obj)
        {
            string[] strResult = new string[2];
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("UserRightsValidates");
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchLoginId", DbType.String, obj.LoginId);
            DataAccess.DataAccess.db.AddInParameter(dbCom, "@vchFormName", DbType.String, obj.FormName);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitReadAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitWriteAccess", DbType.Boolean, 0);
            //DataAccess.DataAccess.db.AddOutParameter(dbCom, "@bitApproveAccess", DbType.Boolean, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@intUserid", DbType.Int64, 0);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchEmailId", DbType.String, 100);
            DataAccess.DataAccess.db.AddOutParameter(dbCom, "@vchUserName", DbType.String, 100);
            DataAccess.DataAccess.GetScalar(dbCom);

            strResult[0] = (dbCom.Parameters["@bitReadAccess"].Value).ToString();
            strResult[1] = (dbCom.Parameters["@bitWriteAccess"].Value).ToString();
            //strResult[2] = (dbCom.Parameters["@intUserId"].Value).ToString();
            //strResult[3] = (dbCom.Parameters["@vchEmailId"].Value).ToString();
            ////strResult[4] = (dbCom.Parameters["@bitApproveAccess"].Value).ToString();
            //strResult[4] = (dbCom.Parameters["@vchUserName"].Value).ToString();
            return strResult;


        }

       
      
   
   


    }
}
