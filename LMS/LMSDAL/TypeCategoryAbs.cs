﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
    public abstract class TypeCategoryAbs
    {
        public abstract DataSet GetCategoryDetails();
        public abstract DataSet GetStatusDescriptionInfo();
        public abstract int SaveCategoryType(CategoryInfo obj);
        public abstract string[] Accessverifyer(CategoryInfo obj);

    }
}
