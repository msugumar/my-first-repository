﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LMSBLL;

namespace LMSDAL
{
    public abstract class SearchAbs
    {
        public abstract DataSet GetSearchInfo();
        public abstract DataSet GetTitleInfo();
        public abstract DataSet GetCategoryInfo();
        public abstract DataSet GetItemTypeInfo();
        public abstract DataSet GetAuthorInfo();
        public abstract DataSet PutSearchDetail(SearchInfo obj);
        public abstract DataSet GetFiterByItemTypeInfo(SearchInfo obj);
        public abstract DataSet GetFiterByCategoryInfo(SearchInfo obj);
        public abstract DataSet GetFiterByTitleInfo(SearchInfo obj);
        public abstract DataSet GetFiterByAuthorInfo(SearchInfo obj);
        public abstract string[] Accessverifyer(SearchInfo obj);
    }
}
