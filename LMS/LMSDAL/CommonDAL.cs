﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using System.Data;
using LMSBLL;
using System.Data.SqlClient;

namespace LMSDAL
{
    public class CommonDAL : CommonAbs
    {
        System.Data.Common.DbCommand dbCom;
        public override DataSet GeActiveStatusList()
        {

            //try
            //{
            dbCom = DataAccess.DataAccess.db.GetStoredProcCommand("StatusActiveList_Get");
            return DataAccess.DataAccess.GetDataSet(dbCom);
            //}
            //catch (Exception ex)
            //{
            //    //ErrorHandler.RaiseError(ex, strPath);

            //}
        } //GeActiveStatusList ends
    }//class ends
}//namespace ends
