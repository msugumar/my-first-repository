﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMSBLL
{
    public class ItemMaintainInfo
    {


        private int _intItemId;
        private int _intItemTypeId;
        private string _strTitle;
        private int _intCategoryId;
        private int _intDeptId;
        private decimal _intPrice;
        private string _strAuthor;
        private int _intStatusId;
        private string _strLoginId;
        private string _strFormName;

        public int itemId
        {
            get { return _intItemId; }
            set { _intItemId = value; }
        }
        
        public int ItemTypeId
        {
            get { return _intItemTypeId; }
            set { _intItemTypeId= value; }
        }
        public string title
        {
            get { return _strTitle; }
            set { _strTitle = value; }
        }
        
        public int category
        {
            get { return  _intCategoryId; }
            set {  _intCategoryId = value; }
        }
        public int DeptId
        {
            get { return  _intDeptId; }
            set { _intDeptId = value; }
        }
        public decimal price
        {
            get { return _intPrice; }
            set { _intPrice = value; }
        }
        
         public string author
        {
            get { return _strAuthor;  }
            set { _strAuthor = value; }
        }

         public int StatusId
        {
            get { return   _intStatusId;  }
            set { _intStatusId = value; }
        }
         public string LoginId
         {
             get { return _strLoginId; }
             set { _strLoginId = value; }

         }
         public string FormName 
         {
             get { return _strFormName; }
             set { _strFormName = value; }
         }






    }
}
