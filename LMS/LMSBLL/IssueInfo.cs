﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LMSBLL
{
    public class IssueInfo
    {
        private int _intTransactionId;
        private int _intItemId;
        private DateTime _dtIssueDate;
        private DateTime _dtDueDate;
        private DateTime _dtReturnDate;
        private int _intStatusId;
        private int _intReturnedFlag;
        private int _intItemTypeId;
        private int _intUserId;
        private int _intSerialNumber;
        private DateTime _dtRenewalDate;
        private int _intRenewalId;
        private string _strLoginId;
        private string _strFormName;
        public int TransactionId
        {
            get { return _intTransactionId; }
            set { _intTransactionId = value; }
        }
        public int ItemId
        {
            get { return _intItemId; }
            set { _intItemId = value; }
        }
        public int UserId
        {
            get { return _intUserId; }
            set { _intUserId = value; }
        }
        public DateTime IssueDate
        {
            get { return _dtIssueDate; }
            set { _dtIssueDate = value; }
        }
        public DateTime DueDate
        {
            get { return _dtDueDate; }
            set { _dtDueDate = value; }
        }
        public DateTime ReturnDate
        {
            get { return _dtReturnDate; }
            set { _dtReturnDate = value; }
        }
        public int StatusId
        {
            get { return _intStatusId; }
            set { _intStatusId = value; }
        }
        public int ReturnedFlag
        {
            get { return _intReturnedFlag; }
            set { _intReturnedFlag = value; }
        }
        public int ItemTypeId
        {
            get { return _intItemTypeId; }
            set { _intItemTypeId = value; }
        }
        public int SerialNumber
        {
            get { return _intSerialNumber; }
            set { _intSerialNumber = value; }
        }
        public DateTime RenewalDate
        {
            get { return _dtRenewalDate; }
            set { _dtRenewalDate = value; }
        }
        public int RenewalId
        {
            get { return _intRenewalId; }
            set { _intRenewalId = value; }
        }
        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }
    }
}
