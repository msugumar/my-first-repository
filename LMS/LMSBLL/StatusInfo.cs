﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMSBLL
{
    public class StatusInfo
    {
        private int _intStatusId;
        private string _strStatusText;
        private string _strLoginId;
        private string _strFormName;
        //private string _strStatusDescription;

        public int StatusId
        {
            get { return _intStatusId; }
            set { _intStatusId = value; }
        }

        public string status
        {
            get { return _strStatusText; }
            set { _strStatusText = value; }
        }

        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }
       // public string StatusDescription
       // {
         //   get { return _strStatusDescription; }
         //   set { _strStatusDescription = value; }
       // }
      
    }
}
