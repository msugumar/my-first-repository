﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMSBLL
{
    public class RolesInfo
    {
        #region "Declaration....."
        private int _roleid;
        private string _role;
        private int _statusinfo;
        private string _forms;
        private int _PrivilageId;
        private string _FormId;
        private string _bitReadAccess;
        private string _bitWriteAccess;
        //private int _Output;
        private string _strLoginId;
        private string _strFormName;
        #endregion

        #region  "AssigningVAlues"
        public string bitWriteAccess
        {
            get { return _bitWriteAccess; }
            set { _bitWriteAccess = value; }
        }
        public string bitReadAccess
        {
            get { return _bitReadAccess; }
            set { _bitReadAccess = value; }
        }
        public int PrivilageId
        {
            get { return _PrivilageId; }
            set { _PrivilageId = value; }
        }
        public string FormId
        {
            get { return _FormId; }
            set { _FormId = value; }
        }
        public string form
        {
            get { return _forms; }
            set { _forms = value; }
        }
        public int roleid
        {
            get { return _roleid; }
            set { _roleid = value; }
        }
        public string rolename
        {
            get
            {
                return _role;
            }
            set { _role = value; }
        }
        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }
         public int statusinfo
        {
            get { return _statusinfo; }
            set { _statusinfo = value; }
        }
         //public int output
         //{
         //    get { return _Output; }
         //    set { _Output = value; }
         //}
        #endregion
        

    }

}
