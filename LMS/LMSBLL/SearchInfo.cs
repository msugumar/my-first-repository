﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMSBLL
{
    public class SearchInfo
    {
        private string _strSearchTitle;
        private string _strSearchCategory;
        private string _strSearchItemType;
        private string _strSearchAuthor;
        private int _intSearchItemID;
        private int _intSearchCategoryID;
        private string _strLoginId;
        private string _strFormName;
        
        public int SearchItemID
        {
            get { return _intSearchItemID; }
            set { _intSearchItemID = value; }
        }

        public int SearchCategoryID
        {
            get { return _intSearchCategoryID; }
            set { _intSearchCategoryID = value; }
        }

        public string SearchTitle
        {
            get { return _strSearchTitle; }
            set { _strSearchTitle = value; }
        }

        public string SearchCategory
        {
            get { return _strSearchCategory; }
            set { _strSearchCategory = value; }
        }

        public string SearchItemType
        {
            get { return _strSearchItemType; }
            set { _strSearchItemType = value; }
        }

        public string SearchAuthor
        {
            get { return _strSearchAuthor; }
            set { _strSearchAuthor = value; }
        }

        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }


    }
}
