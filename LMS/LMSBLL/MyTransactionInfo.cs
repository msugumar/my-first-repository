﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMSBLL
{
        
    
    public class MyTransactionInfo
    
    
    {
        private DateTime _StartDate;
        private DateTime _EndDate;
       private string _ItemStatus;
       private string _strLoginId;
       private string _strFormName;

        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        public string ItemStatus
        {
            get { return _ItemStatus; }
            set { _ItemStatus = value; }
        }
        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }

       
        }
}
   


