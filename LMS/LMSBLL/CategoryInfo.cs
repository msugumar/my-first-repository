﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LMSBLL
{
    public class CategoryInfo
    {
        private int _intCategoryId;
        private string _strCategory;
        private string _strStatusDescription;
        private string _strCategoryDescription;
        private int _intStatusID;
        private string _strLoginId;
        private string _strFormName;
        public int CategoryId
        {
            get { return _intCategoryId; }
            set { _intCategoryId = value; }
        }

        public string Category
        {
	        get { return _strCategory; }
	        set { _strCategory = value; }
        }
        public int StatusID
        {
            get { return _intStatusID; }
            set { _intStatusID = value; }
        }
        public string StatusDescription
        {
            get { return _strStatusDescription; }
            set { _strStatusDescription = value; }
        }
        public string CategoryDescription
        {
            get { return _strCategoryDescription; }
            set { _strCategoryDescription = value; }
        }
        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }
    } //class ends
}//nameshpace ends

