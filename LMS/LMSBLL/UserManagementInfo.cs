﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMSBLL
{
    public class UserManagementInfo
    {
        private string _strUserID;
        private string _strUserRole;
        private string _strRoleID;
        private string _strLoginId;
        private string _strFormName;

        public string UserID
        {
            get { return _strUserID; }
            set { _strUserID = value; }
        }

        public string UserRole
        {
            get { return _strUserRole; }
            set { _strUserRole = value; }
        }
        public string RoleID
        {
            get { return _strRoleID; }
            set { _strRoleID = value; }
        }
        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }

    }
}
