﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMSBLL
{
    public class ItemTypeInfo
    {
        private int _intItemTypeId;
        private string _strItemType;
        private string _strStatusDescription;
        private int _intStatusID;
        private string _strLoginId;
        private string _strFormName;

        public int ItemTypeId
        {
            get { return _intItemTypeId; }
            set { _intItemTypeId = value; }
        }

        public string ItemType
        {
            get { return _strItemType; }
            set { _strItemType = value; }
        }
        public int StatusID
        {
            get { return _intStatusID; }
            set { _intStatusID = value; }
        }

        public string StatusDescription
        {
            get { return _strStatusDescription; }
            set { _strStatusDescription = value; }
        }
        public string LoginId
        {
            get { return _strLoginId; }
            set { _strLoginId = value; }

        }
        public string FormName
        {
            get { return _strFormName; }
            set { _strFormName = value; }
        }
    }
}
