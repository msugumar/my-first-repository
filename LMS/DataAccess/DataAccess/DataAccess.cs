﻿using Microsoft.CSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace DataAccess
{
    public class DataAccess
    {
        
        public static Database db = DatabaseFactory.CreateDatabase("con");
        public static string strpath = "";
        public static DataSet GetDataSet(System.Data.Common.DbCommand dbcom)
        {

            DataSet ds = new DataSet();
            dbcom.CommandTimeout = 0;
            try
            {
                ds = db.ExecuteDataSet(dbcom);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;

        }
        public static string GetScalar(System.Data.Common.DbCommand dbcom)
        {

            string result = "";
            dbcom.CommandTimeout = 0;
            try
            {
                result = db.ExecuteScalar(dbcom).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result.Trim();

        }

        public static String ExecuteNonQuery(System.Data.Common.DbCommand dbcom)
        {

            String result = "0";
            dbcom.CommandTimeout = 0;
            try
            {
                result = db.ExecuteNonQuery(dbcom).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public static IDataReader ExecuteReader(System.Data.Common.DbCommand dbcom)
        {

            IDataReader result = null;
            dbcom.CommandTimeout = 0;
            try
            {
                result = db.ExecuteReader(dbcom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}

