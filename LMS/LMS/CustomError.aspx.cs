﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CustomError : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "We are unable to process your request";
        lblMessage.Text = "Please contact LMS Administrator with the Below Error Reference ID.";
        lblErrorNumber.Text = "Your Reference Error ID is " + Convert.ToInt32(Session["ErrorID"]);
    }
}
