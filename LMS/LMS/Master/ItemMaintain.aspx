<%@ Page Title="Item" Language="C#" Theme="LMS" MasterPageFile="~/LMSMaster.master"
    AutoEventWireup="true" CodeFile="ItemMaintain.aspx.cs" Inherits="Master_ItemMaintain" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="Server">
    <asp:UpdatePanel runat="server" ID="upItem">
        <ContentTemplate>
            <table style="width: 100%">
                <tr class="TRHeader">
                    <td align="center" colspan="4">
                        <span>Item</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbItemtype" runat="server" Text="ItemType" CssClass="label"></asp:Label>
                         <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddItemtype" runat="server" ToolTip="Select The Item Type" TabIndex="1"
                            SkinID="ddlCustomer">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="lblTitels" runat="server" Text="Title" CssClass="label"></asp:Label>
                         <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" TabIndex="2" ToolTip="Enter The Title"
                            SkinID="TxtMultiLine"></asp:TextBox>
                         </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="label"></asp:Label>
                         <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddStatus" runat="server" ToolTip="Select The Status" TabIndex="3"
                            SkinID="ddlCustomer">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="lblCategory" runat="server" Text="Category" CssClass="label"></asp:Label>
                         <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddCategory" runat="server" ToolTip="Select The Category" TabIndex="4"
                            SkinID="ddlCustomer">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                <td>
                        <asp:Label ID="lblAuthour" runat="server" Text="Author" CssClass="label"></asp:Label>
                         <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAuthour" runat="server" ToolTip="Enter The Authour" TabIndex="5"
                            SkinID="TxtMultiLine"></asp:TextBox>
                        
                    </td>
       
                   <td>
                        <asp:Label ID="lblPrice" runat="server" Text="Price" CssClass="label"></asp:Label>
                        <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrice" runat="server" ToolTip="Enter The Price" TabIndex="6"
                            SkinID="TxtMultiLine"></asp:TextBox>
                        
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom, Numbers"
                            ValidChars="123456789.50" TargetControlID="txtPrice">
                        </cc1:FilteredTextBoxExtender>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                     <td>
                        <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="label"></asp:Label>
                        <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddDepartment" runat="server" ToolTip="Select The Department"
                            TabIndex="7" SkinID="ddlCustomer">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="left">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button
                            ID="btnSave" runat="server" ToolTip="Click here to Save Item Details" Text="Save"
                            OnClick="btnSave_Click" OnClientClick="return validate()" Width="75px" />
                        &nbsp; &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" ToolTip="Click Here To Cancel"
                            OnClick="btnCancel_Click" Width="75px" />
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td />
                </tr>
                <tr>
                    <td />
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="upItem" ID="PageProgress"
                            DisplayAfter="1">
                            <ProgressTemplate>
                                <table align="center">
                                    <tr>
                                        <td align="center">
                                            <asp:Image runat="server" ImageUrl="~/images/Progress.gif" Height="20px" Width="20px"
                                                ID="imgWait"></asp:Image>
                                        </td>
                                        <td>
                                            <span>
                                                <asp:Label runat="server" Text="Processing...Please Wait!" ID="lblProcessingAnim"></asp:Label></span>
                                        </td>
                                    </tr>
                                </table>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:GridView ID="gvItemDetailForm" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="silver" HeaderStyle-VerticalAlign="Top"
                            OnPageIndexChanging="gvItemDetailForm_PageIndexChanging" OnSorting="gvItemDetailForm_Sorting"
                            ToolTip="Item Maintenance Form" OnRowCommand="gvItemDetailForm_RowCommand" BorderColor="#e8c8af"
                            BorderWidth="1px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>
                                <asp:BoundField DataField="ItemType" HeaderText="Item Type" SortExpression="ItemType">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="40" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="120" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="120" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="20" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="40" />
                                </asp:BoundField>
                                <asp:TemplateField Visible="False" HeaderText="ITEM_ID">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblprice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Price") %>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Author" HeaderText="Author" SortExpression="Author">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="70" />
                                </asp:BoundField>
                                <asp:TemplateField Visible="False" HeaderText="ITEM_ID">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblItemid" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemId") %>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById("<%= ddItemtype.ClientID %>").selectedIndex == "") {
                alert("Please Fill The Item Type Its Empty Now");
                document.getElementById("<%=ddItemtype.ClientID%>").focus();
                return false;
            }
            if (document.getElementById('<%=txtTitle.ClientID%>').value == '') {
                alert("Please Fill The Text Title Its Empty Now");
                document.getElementById('<%=txtTitle.ClientID%>').focus();
                return false;
            }
            if (document.getElementById("<%=ddStatus.ClientID%>").selectedIndex == "") {
                alert("Please Fill The Status Its Empty Now");
                document.getElementById("<%=ddStatus.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=ddCategory.ClientID%>").selectedIndex == "") {
                alert("Please Fill The Category Its Empty Now");
                document.getElementById("<%=ddCategory.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txtAuthour.ClientID%>").value == "") {
                alert("Please Fill The Author Its Empty Now");
                document.getElementById("<%=txtAuthour.ClientID%>").focus();
                return false;
            }
          
            if (document.getElementById("<%=txtPrice.ClientID%>").value == "") {
                alert("Please Fill The Price Its Empty Now");
                document.getElementById("<%=txtPrice.ClientID%>").focus();
                return false;
            }
             if (document.getElementById("<%=ddDepartment.ClientID%>").selectedIndex == "") {
                alert("Please Fill The Department Its Empty Now");
                document.getElementById("<%=ddDepartment.ClientID%>").focus();
                return false;
            }

            
        }

        function pricedec() {
            var txtPrice1 = document.getElementById("<%=txtPrice.ClientID%>").value;  // here in paranthesis, give your textbox name.                    
            var splitwords = txtPrice1.split(".");
            {

                //                if (splitwords.length > 2) {
                //                    alert("Wrong Format");
                //                    document.getElementById("<%=txtPrice.ClientID%>").focus();
                //                    return false;

                //                }
                if (splitwords.length > 1) {
                    if (splitwords[1].length > 1) {

                        alert("Enter Only Two Decimals");
                        document.getElementById("<%=txtPrice.ClientID%>").focus();

                        return false;
                    }

                }
            }
        }
    </script>

</asp:Content>
