﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using LMSBLL;
using LMSDAL;
using ExceptionalHandling;
using AjaxControlToolkit;


public partial class Master_ItemTypeMaster : System.Web.UI.Page
{
    public String strErrorMessage;
    public String strPage;
    public String[] strResult;
    public LMSBLL.ItemTypeInfo objInfo;
    public LMSDAL.ItemTypeDAL objDAL;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PopulateItemTypeDetails();
            PopulateDropDown();
            AccessIdentifier();
        } //if not post back ends

    }//Page load ends

    private void PopulateItemTypeDetails()
    {
        try
        {
            objDAL = new LMSDAL.ItemTypeDAL();

            DataView dvwItemType = new DataView();   //dvwItemType
            if ((ViewState["sortExpr"]) != null)
            {
                dvwItemType.Table = objDAL.GetItemTypeInfo().Tables[0];
                dvwItemType.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
            }
            else
            {
                dvwItemType = objDAL.GetItemTypeInfo().Tables[0].DefaultView;
            }

            grdItemTypeMaster.DataSource = dvwItemType;
            grdItemTypeMaster.DataBind();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objDAL = null;
        }
    } //PopulateItemTypeDetails ends

    protected void grdItemType_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)   //grdItemType_PageIndexChanging
    {
        try
        {
            grdItemTypeMaster.PageIndex = e.NewPageIndex;
            PopulateItemTypeDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    } //gridview page index changing ends


    protected void grdItemType_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            ViewState["sortExpr"] = e.SortExpression;
            if ((String)ViewState["sortDirection"] == "ASC")
            {
                ViewState["sortDirection"] = "DESC";
            }
            else
            {
                ViewState["sortDirection"] = "ASC";
            }
            PopulateItemTypeDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    }

    private void Cancel()
    {
        try
        {
            ViewState["ItemTypeID"] = 0;
            txtStatusDescription.Text = "";
            txtItem_Type.Text = "";
            ddStatus.SelectedIndex = ddStatus.Items.IndexOf(ddStatus.Items.FindByText("--Select Here--"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    }   //cancel method ends

    protected void butCancel_Click(object sender, System.EventArgs e)
    {
        Cancel();
    }

    protected void butSave_Click(object sender, System.EventArgs e)
    {
        //if (txtItem_Type.Text.Length > 0 && ddStatus.SelectedValue != "Select Here")
        //{
            SaveItemTypeInfo();
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Enter the Valid Credentials');</script>", false);
        //}
    }

    protected void grdItemTypeMaster_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                ddStatus.SelectedIndex = ddStatus.Items.IndexOf(ddStatus.Items.FindByText(((grdItemTypeMaster.Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text.ToString()))));
                txtStatusDescription.Text = ((System.Web.UI.WebControls.Label)(grdItemTypeMaster.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblDescription"))).Text.ToString();
                txtItem_Type.Text = (grdItemTypeMaster.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text).ToString();
                ViewState["ItemTypeID"] = ((System.Web.UI.WebControls.Label)(grdItemTypeMaster.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblItemTypeID"))).Text.ToString();
                //ddStatusDescription.SelectedIndex = ddStatusDescription.Items.IndexOf(ddStatusDescription.Items.FindByValue(((System.Web.UI.WebControls.Label)(grdItemTypeMaster.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblStatusDescription"))).Text.ToString()));
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    }

    protected void SaveItemTypeInfo()
    {
        try
        {            
            objInfo = new LMSBLL.ItemTypeInfo();
            objInfo.ItemTypeId = Convert.ToInt32(ViewState["ItemTypeID"]);
            objInfo.ItemType = txtItem_Type.Text;
            objInfo.StatusDescription= txtStatusDescription.Text;
            objInfo.StatusID = Convert.ToInt32(ddStatus.SelectedValue);           
            objDAL = new LMSDAL.ItemTypeDAL();
            int intOutput = objDAL.InsertItemType(objInfo);
            PopulateItemTypeDetails();

            switch (intOutput)
            {
                case 0 :
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Error has Occured.');</script>", false);
                    Cancel();
                    break;
                case 1 :
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Item Type Details Inserted successfully');</script>", false);
                    Cancel();
                    break;
                case 2 :
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Item Type Details Updated successfully');</script>", false);
                    Cancel();
                    break;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    } //saveitemtypeinfo ends
   
   
    public void PopulateDropDown()
    {
        try
        {
            objDAL = new LMSDAL.ItemTypeDAL();
            ddStatus.DataSource = objDAL.GetStatusDescriptionInfo().Tables[0];
            ddStatus.DataTextField = "Status";
            ddStatus.DataValueField = "StatusId"; 
            ddStatus.DataBind();
            ddStatus.Items.Insert(0, new ListItem("--Select Here--", "0"));
            //ddStatus.Items.Insert(0, "Select Here");
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    }
    private void AccessIdentifier()
    {
        try
        {
            objInfo = new LMSBLL.ItemTypeInfo();
            objDAL = new LMSDAL.ItemTypeDAL();
            //objDAL = new LMSDAL.ItemMaintainDal();
            strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
            WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            objInfo.LoginId = ExtractUserName(struser.Identity.Name);
            int intLen = 0;
            intLen = strPage.IndexOf(".");
            objInfo.FormName = strPage.Substring(0, intLen);
            strResult = objDAL.Accessverifyer(objInfo);

            if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
            {
                butSave.Enabled = true;
            }
            else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
            {
                butSave.Enabled = false;
            }
            else
            {
                AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane2");
                LinkButton lnkItemTypeMaster = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkItemTypeMaster");
                Response.Redirect("~/UnAuthorization.aspx?file=" + lnkItemTypeMaster.Text.ToString(), false);
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    public string ExtractUserName(string path)
    {
        /*  try
          {*/
        string[] userPath = path.Split(new char[] { '\\' });
       return userPath[userPath.Length - 1];
        /*      }
              catch (Exception ex)
         * 
              {
                  ErrorHandler.RaiseError(ex, strLogError);
              } */
    }
   


}

