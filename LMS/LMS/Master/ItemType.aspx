<%@ Page Title="Item Type" Language="C#" Theme="LMS" MasterPageFile="~/LMSMaster.master"
    AutoEventWireup="true" CodeFile="ItemType.aspx.cs" Inherits="Master_ItemTypeMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="server">

    

    <!--- Display portion started--->
    <asp:UpdatePanel runat="server" ID="updContract">
        <ContentTemplate>
            <table style="width: 100%; font-family: Verdana; font-size: 11px;" border="0">
                <tr class="TRHeader">
                    <td align="center" colspan="4">
                        <span>Item Type</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="100">
                        <asp:Label ID="lblItem_Type" runat="server" Text="Item Type" CssClass="label" />
                         <span style="font-weight: bold; color: Red;">*</span>
                      
                    </td>
                    <td>
                        <asp:TextBox ID="txtItem_Type" runat="server" ToolTip="Select the ItemType" TabIndex="1"
                            MaxLength="200" SkinID="TxtMultiLine" />
                        <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="txtItem_Type" WatermarkText="ENTER THE ITEM TYPE" WatermarkCssClass="watermarked" runat="server">
                    </cc1:TextBoxWatermarkExtender>--%>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblStatusDescription" runat="server" Text="Status" CssClass="label" />
                        <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddStatus" runat="server" ToolTip="Select the Status" TabIndex="2"
                            SkinID="ddlCustomer">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td />
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="label" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtStatusDescription" runat="server" MaxLength="2000" TabIndex="3"
                            ToolTip="SELECT THE StatusDescription" TextMode="MultiLine" SkinID="TxtMultiLine" />
                        <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" TargetControlID="txtStatusDescription" WatermarkText="ENTER THE ITEM  DESCRIPITION" WatermarkCssClass="watermarked" runat="server" >
                        </cc1:TextBoxWatermarkExtender>--%>
                    </td>
                </tr>
            </table>
            <table align="left">
                <tr>
                    <td align="center">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button
                            ID="butSave" runat="server" Text="Save" TabIndex="3" ToolTip="Click here to save category details"
                            OnClick="butSave_Click" OnClientClick="return validateControls()" ></asp:Button>
                        &nbsp;<asp:Button ID="butCancel" runat="server" Text="Cancel" TabIndex="4" ToolTip="Click here to cancel"
                            OnClick="butCancel_Click"></asp:Button>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <table align="left" width="100%">
                <tr >
                    <td>
                        <asp:GridView ID="grdItemTypeMaster" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="silver" HeaderStyle-VerticalAlign="Top"
                            OnPageIndexChanging="grdItemType_PageIndexChanging" OnSorting="grdItemType_Sorting"
                            ToolTip="ITEM TYPR FORM" OnRowCommand="grdItemTypeMaster_RowCommand" BorderColor="#e8c8af"
                            BorderWidth="1px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>
                                <asp:BoundField DataField="ItemType" HeaderText="Item Type" SortExpression="ItemType">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="70" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="90" />
                                </asp:BoundField>
                                <asp:TemplateField Visible="False" HeaderText="ItemTypeID">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblItemTypeID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ItemTypeId") %>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="False" HeaderText="Description">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StatusDescription") %>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr><td /></tr>
            </table>
            
            <!--- id="tblPageLevel --->
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--- End display portion --->
    <script language="javascript" type="text/javascript">
        function validateControls() {
            if (document.getElementById('<%=txtItem_Type.ClientID%>').value == "") {
                alert("PLEASE FILL THE TEXT ITEM TYPES ITS EMPTY NOW");
                document.getElementById('<%=txtItem_Type.ClientID%>').focus();
                return false;
            }
            if (document.getElementById("<%=ddStatus.ClientID%>").selectedIndex == "") {
                alert("PLEASE FILL THE STATUS ITS EMPTY NOW");
                document.getElementById("<%=ddStatus.ClientID%>").focus();
                return false;
            }
        }
    </script>
</asp:Content>
