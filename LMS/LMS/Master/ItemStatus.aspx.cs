﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using ExceptionalHandling;
using LMSBLL;
using LMSDAL;
using AjaxControlToolkit;


public partial class Master_MasterStatus : System.Web.UI.Page
{
    public string strErrorMessage;
     public string strPage;
    public string[] strResult;
    public LMSBLL.StatusInfo objInfo;
    public LMSDAL.StatusDAL objDAL;


    #region "Page Load"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PopulateStatusDetails();
            AccessIdentifier();
            // PopulateReferenceId();
        }
    }
    #endregion

    #region "Populate Status Details"

    private void PopulateStatusDetails()
    {
        try
        {
            objDAL = new LMSDAL.StatusDAL();

            DataView dvStatus = new DataView();
            if ((ViewState["sortExpr"]) != null)
            {
                dvStatus.Table = objDAL.GetStatusInfo().Tables[0];
                dvStatus.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
            }
            else
            {
                dvStatus = objDAL.GetStatusInfo().Tables[0].DefaultView;
            }

            grdViewStatus.DataSource = dvStatus;
            grdViewStatus.DataBind();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    } //Populate Status Details ends

    #endregion

    #region "Page Index Changing"
    protected void grdStatus_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            grdViewStatus.PageIndex = e.NewPageIndex;
            PopulateStatusDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    } //gridview page index changing ends
    #endregion

    #region "GridView Sorting"
    protected void grdstatus_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            ViewState["sortExpr"] = e.SortExpression;
            if ((String)ViewState["sortDirection"] == "ASC")
            {
                ViewState["sortDirection"] = "DESC";
            }
            else
            {
                ViewState["sortDirection"] = "ASC";
            }
            PopulateStatusDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }
    //grid view sorting ends
    #endregion

    #region " Grid View Row Command"
    protected void grdStatus_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
        objInfo = new LMSBLL.StatusInfo();
        objDAL = new LMSDAL.StatusDAL();

            if (e.CommandName == "Select")
            {
                
                ViewState["StatusID"] = ((System.Web.UI.WebControls.Label)(grdViewStatus.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblStatusId"))).Text.ToString();
                //txtStatusText.Text = ((grdViewStatus.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text));
                int intRowIndex = Convert.ToInt32(e.CommandArgument);
                GridViewRow grdView = grdViewStatus.Rows[intRowIndex];
                txtStatusText.Text = grdView.Cells[1].Text;
                //txtStatusText.Text = ((System.Web.UI.WebControls.Label)(grdViewStatus.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblStatusTxt"))).Text.ToString();
                
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }

    //gridviewStatus_RowCommand ends
    #endregion

    #region "Button Save Event"
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtStatusText.Text.Length > 0)
        {
            SaveStatusInfo();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Enter the Status');</script>", false);
        }

    }
    //CLick Events Ends
    #endregion

    #region "Button Cancel Event"
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Cancel();
    }
    //Cancel Events Ends
    #endregion

    #region " Button Save Function "
    protected void SaveStatusInfo()
    {
        



        try
        {

           // btnSave.Attributes.Add("onclick", "confirmation()");
            objInfo = new LMSBLL.StatusInfo();
            objInfo.status = txtStatusText.Text;
            objInfo.StatusId = Convert.ToInt32(ViewState["StatusID"]);
            objDAL = new LMSDAL.StatusDAL();
            int output = objDAL.InsertStatus(objInfo);
            PopulateStatusDetails();
            switch (output)
            {
               
                case 1:
                   
                    //lblErrorMessage.Text = "Inserted successfully";
                   
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "msg", "<script>alert('The Status Inserted Successfully');</script>", false);
                    break;
                case 2:
                   // lblErrorMessage.Text = "Updated successfully";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "msg", "<script>alert('The Status Updated Successfully');</script>", false);
                    break;
                case 3:
                   // lblErrorMessage.Text = "An error has occured";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "msg", "<script>alert('Status Already Exist.Please Enter New Status!');</script>", false);
                    break;
                case 4:
                    // lblErrorMessage.Text = "An error has occured";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "msg", "<script>alert('Status Cannot be Null!');</script>", false);
                    break;
            }
            txtStatusText.Text = "";
        }

        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }

    }

    //Save Status Information Ends
    #endregion

    #region "Button Cancel Function"
    private void Cancel()
    {
        // txtStatusDesc.Text = string.Empty;
        txtStatusText.Text = string.Empty;
    }
    //cancel method ends
    #endregion

     private void AccessIdentifier()
    {
        try {
        objInfo = new LMSBLL.StatusInfo();
        objDAL = new LMSDAL.StatusDAL();
        //objDAL = new LMSDAL.ItemMaintainDal();
        strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
        WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
        objInfo.LoginId = ExtractUserName(struser.Identity.Name);
        int intLen = 0;
        intLen = strPage.IndexOf(".");
        objInfo.FormName = strPage.Substring(0, intLen);
        strResult = objDAL.Accessverifyer(objInfo);

        if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
        {
            btnSave.Enabled = true;
        }
        else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
        {
            btnSave.Enabled = false;
        }
        else 
        {
            AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane2");
            LinkButton lnkItemStatus = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkItemStatus");
            Response.Redirect("~/UnAuthorization.aspx?file=" + lnkItemStatus.Text.ToString(), false);
            return;
        }
        } 
        catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();
                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
                Session["ErrorID"] = intError.ToString();
                Response.Redirect("CustomError.aspx");
            }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    public string ExtractUserName(string path)
    {
       //  try
         // {
        string[] userPath = path.Split(new char[] { '\\' });
        return userPath[userPath.Length - 1];
            // }
             // catch (Exception ex)
         
            //  {
                //  ErrorHandler objErrorHandler = new ErrorHandler();
                //  objErrorHandler.RaiseError(ex, strErrorMessage);
                  //ErrorHandler.RaiseError(ex, strLogError);
              //} 
    }



}








