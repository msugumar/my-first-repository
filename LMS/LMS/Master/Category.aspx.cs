﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using ExceptionalHandling;
using LMSBLL;
using LMSDAL;
using AjaxControlToolkit;

public partial class Master_Category : System.Web.UI.Page
{
    public string strErrorMessage;
    public string strPage;
    public string[] strResult;
    public LMSBLL.CategoryInfo objInfo;
    public LMSDAL.CategoryDAL objDAL; 
    protected void Page_Load(object sender, EventArgs e)
    {
        //txtCategoryName.Attributes.Add("onKeyDown", "return spacevalidate(event)");
        if (!Page.IsPostBack)
        {
            PopulateDropDown();
            PopulateCategoryDetails();
            AccessIdentifier();
        } 
    }

    private void PopulateCategoryDetails()
    {
        try
        {
            objInfo = new LMSBLL.CategoryInfo();
            objDAL = new LMSDAL.CategoryDAL();

            DataView dvCategory = new DataView();
            if ((ViewState["sortExpr"]) != null)
            {
                dvCategory.Table = objDAL.GetCategoryDetails().Tables[0];
                dvCategory.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
            }
            else
            {
                dvCategory = objDAL.GetCategoryDetails().Tables[0].DefaultView;
            }

            grdCategoryDetails.DataSource = dvCategory;
            grdCategoryDetails.DataBind();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    } 
    #region "GridViewEvents..."


    protected void grdCategoryDetails_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            grdCategoryDetails.PageIndex = e.NewPageIndex;
            PopulateCategoryDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    } //gridview page index changing ends


    protected void grdCategoryDetails_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            ViewState["sortExpr"] = e.SortExpression;
            if ((String)ViewState["sortDirection"] == "ASC")
            {
                ViewState["sortDirection"] = "DESC";
            }
            else
            {
                ViewState["sortDirection"] = "ASC";
            }
            PopulateCategoryDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    } //gvCategory_Sorting ends 
    protected void grdCategoryDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                ddStatus.SelectedIndex = ddStatus.Items.IndexOf(ddStatus.Items.FindByText(((grdCategoryDetails.Rows[Convert.ToInt32(e.CommandArgument)].Cells[3].Text.ToString()))));
                //txtCategoryDesc.Text = (grdCategoryDetails.Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text).ToString();
                 txtCategoryName.Text= (grdCategoryDetails.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text).ToString();
                 ViewState["CategoryId"] = ((System.Web.UI.WebControls.Label)(grdCategoryDetails.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblCategoryId"))).Text.ToString();
                //ddStatusDescription.SelectedIndex = ddStatusDescription.Items.IndexOf(ddStatusDescription.Items.FindByValue(((System.Web.UI.WebControls.Label)(grdItemTypeMaster.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblStatusDescription"))).Text.ToString()));
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    }
    #endregion


    private void Cancel()
    {
        //txtCategoryDesc.Text = "";
        ddStatus.SelectedIndex = ddStatus.Items.IndexOf(ddStatus.Items.FindByText("--Select Here--"));
        txtCategoryName.Text = "";
        ViewState["CategoryId"]=0;
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Cancel();
    }
    public void PopulateDropDown()
    {
        try
        {
            objDAL = new LMSDAL.CategoryDAL();
            ddStatus.DataSource = objDAL.GetStatusDescriptionInfo().Tables[0];
            ddStatus.DataTextField = "Status";
            ddStatus.DataValueField = "StatusId";
            ddStatus.DataBind();
            ddStatus.Items.Insert(0, new ListItem("--Select Here--", "0"));
            //ddStatus.Items.Insert(0, "Select Here");
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    }
    protected void butSave_Click(object sender, System.EventArgs e)
    {
        SaveCategoryInfo();
    }
    protected void SaveCategoryInfo()
    {
        try
        {
            objInfo = new LMSBLL.CategoryInfo();
            objInfo.CategoryId = Convert.ToInt32(ViewState["CategoryId"]);
            objInfo.Category = txtCategoryName.Text;
            objInfo.StatusID = Convert.ToInt32(ddStatus.SelectedValue);
            objInfo.CategoryDescription = txtCategoryDesc.Text;
            objDAL = new LMSDAL.CategoryDAL();
            int intOutput = objDAL.SaveCategoryType(objInfo);
            PopulateCategoryDetails();
            switch (intOutput)
            {
                case 0:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Category Already Exists.!');</script>", false);
                    Cancel();
                    break;
                case 1:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Category Details Inserted successfully');</script>", false);
                    Cancel();
                    break;
                case 2:                    
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Category Details Updated successfully');</script>", false);
                    Cancel();
                    break;
                case 3:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Category Cannot Be Null.!');</script>", false);
                    Cancel();
                    break;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    private void AccessIdentifier()
    {
        try
        {
            objInfo = new LMSBLL.CategoryInfo();
            objDAL = new LMSDAL.CategoryDAL();
            strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
            WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            //objInfo.LoginId = ExtractUserName(struser.Identity.Name);      
            objInfo.LoginId = ExtractUserName(struser.Identity.Name);
            int intLen = 0;
            intLen = strPage.IndexOf(".");
            objInfo.FormName = strPage.Substring(0, intLen);
            strResult = objDAL.Accessverifyer(objInfo);
            if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
            {
                btnSave.Enabled = true;
            }
            else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
            {
                btnSave.Enabled = false;
            }
            else
            {
                AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane2");
                LinkButton lnkItemMaintain = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkCategory");
                Response.Redirect("~/UnAuthorization.aspx?file=" + lnkItemMaintain.Text.ToString(), false);
                //return;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    public string ExtractUserName(string path)
    {
        /*  try
          {*/
        string[] userPath = path.Split(new char[] { '\\' });

        return userPath[userPath.Length - 1];
        /*      }
              catch (Exception ex)
         * 
              {
                  ErrorHandler.RaiseError(ex, strLogError);
              } */
    }
}
