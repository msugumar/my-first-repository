﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LMSMaster.master" AutoEventWireup="true" CodeFile="Category.aspx.cs" Theme="LMS" Inherits="Master_Category" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" Runat="Server">
<asp:UpdatePanel runat="server" ID="updContract">
        <ContentTemplate>
            <table width="100%" cellpadding="1" cellspacing="1">
                <tr class="TRHeader">
                    <td align="center" colspan="4">
                        <span>Category Master</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCategoryName" CssClass="label" runat="server" Text="Category" TabIndex="1"></asp:Label>
                        <span style="font-weight: bold; color: Red;">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCategoryName" runat="server" TabIndex="2" ToolTip="Enter the Category Name."
                            MaxLength="100" SkinID="TxtMultiLine"></asp:TextBox>
                    </td>
                 <td>
                    <asp:Label ID="lblStatus" cssClass="label" runat="server" Text="Status" ></asp:Label>
                    <span style="font-weight: bold; color: Red;">*</span>
                </td>
                <td>
                    <asp:DropDownList ID="ddStatus" runat="server" tabIndex="3" SkinID="ddlCustomer" ToolTip = "Select Status">                   
                    </asp:DropDownList>
                </td>             
                   
                </tr>
                <tr>
                     <td>
                        <asp:Label ID="lblCategoryDesc" CssClass="label" runat="server" Text="Descripition" TabIndex="1"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCategoryDesc" runat="server" TabIndex="2" ToolTip="Enter the Category Description." 
                        TextMode="MultiLine" SkinID="TxtMultiLine" Rows="2" Columns="40" MaxLength="200"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnSave" runat="server" OnClientClick="return validateControls()" OnClick="butSave_Click" Text="Save" TabIndex="3" ToolTip="Click here to save category details">
                        </asp:Button>
                        <asp:Button ID="btnCancel" runat="server" OnClientClick="return cancel()" OnClick="btnCancel_Click" Text="Cancel" TabIndex="4" ToolTip="Click here to cancel">
                        </asp:Button>
                    </td>
                </tr>
                </tr>
                <tr>
                    <td colspan="4">
                      <asp:GridView ID="grdCategoryDetails" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="silver" HeaderStyle-VerticalAlign="Top"
                            OnPageIndexChanging="grdCategoryDetails_PageIndexChanging" OnSorting="grdCategoryDetails_Sorting"
                            ToolTip="Category Form" OnRowCommand="grdCategoryDetails_RowCommand">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>
                                <asp:BoundField DataField="CATEGORY" HeaderText="Category" SortExpression="Category">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="90" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CATEGORYDESCRIPTION" Visible="false" HeaderText="Category Description" SortExpression="CategoryDescription">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="90" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="70" />
                                </asp:BoundField>
                                <asp:TemplateField Visible="False" HeaderText="CategoryId">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategoryId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <!--- id="tblPageLevel --->
        </ContentTemplate>
    </asp:UpdatePanel>
  <script language="javascript" type="text/javascript">
        function validateControls() {
            if (document.getElementById('<%=txtCategoryName.ClientID%>').value == "") {
                alert("Please Fill The Category");
                document.getElementById('<%=txtCategoryName.ClientID%>').focus();
                return false;
            }
            if (document.getElementById("<%=ddStatus.ClientID%>").selectedIndex == "") {
                alert("Please Select The Status");
                document.getElementById("<%=ddStatus.ClientID%>").focus();
                return false;
            }
            var a = confirm("Do you really want to Save");
            if (!a) {
                return false;
            }
        }
        function cancel() {
            var a = confirm("Do you really want to Cancel");
            if (!a) {
                return false;
            }
        }
//        function spacevalidate(e) {
//            var KeyID = (window.event) ? event.keyCode : e.which;
//            if ((KeyID == 32)) {
//                return false;
//            }
//            else {
//                return true;
//            }
//        }
    </script>
</asp:Content>

