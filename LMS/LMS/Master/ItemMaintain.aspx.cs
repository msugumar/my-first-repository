﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using ExceptionalHandling;
using LMSBLL;
using LMSDAL;
using AjaxControlToolkit;


public partial class Master_ItemMaintain : System.Web.UI.Page
{
    public string strErrorMessage;
    public string strPage;
    public string[] strResult;
    public LMSBLL.ItemMaintainInfo objInfo;
    public LMSDAL.ItemMaintainDal objDAL;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            AccessIdentifier();

            PopulateItemDetails();
            PopulateItemCategory();

            PopulateItemtype();
            PopoulateDepartment();
            PopulateStatus();
            


        }
        txtPrice.Attributes.Add("onKeyPress", "return pricedec()");
       
    }

    private void AccessIdentifier()
    {
        try {
        objInfo = new LMSBLL.ItemMaintainInfo();
        objDAL = new LMSDAL.ItemMaintainDal();
     
        strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
        WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
        objInfo.LoginId = ExtractUserName(struser.Identity.Name);
        int intLen = 0;
        intLen = strPage.IndexOf(".");
        objInfo.FormName = strPage.Substring(0, intLen);
        strResult = objDAL.Accessverifyer(objInfo);

        if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
        {
            btnSave.Enabled = true;
        }
        else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
        {
            btnSave.Enabled = false;
        }
        else 
        {
            AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane2");
            LinkButton lnkItemMaintain = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkItemMaintain");
            Response.Redirect("~/UnAuthorization.aspx?file=" + lnkItemMaintain.Text.ToString(), false);
            return;
        }
        } 
        catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();
                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
                Session["ErrorID"] = intError.ToString();
                Response.Redirect("CustomError.aspx");




               
            }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    public string ExtractUserName(string path)
    {
        //string str = "Microsoft Community";
        //string strNew = str.subString(10);        //        strNew will be "Community"
        //strNew = str.subString(0, 9);    
        /*  try
          {*/
        
        string[] userPath = path.Split(new char[] { '\\' });

        return userPath[userPath.Length - 1];
        /*      }
              catch (Exception ex)
         * 
              {
                  ErrorHandler.RaiseError(ex, strLogError);
              } */
    }

    protected void gvItemDetailForm_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            GridView gvItem = (GridView)sender;

            switch (e.CommandName)
            {
                case "Select":
                    ViewState["ItemId"] = ((System.Web.UI.WebControls.Label)(gvItemDetailForm.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblitemid"))).Text.ToString();



                    ddItemtype.SelectedIndex = ddItemtype.Items.IndexOf(ddItemtype.Items.FindByText(((gvItem.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text.ToString()))));
                    txtTitle.Text = ((gvItem.Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text.ToString()));
                    ddCategory.SelectedIndex = ddCategory.Items.IndexOf(ddCategory.Items.FindByText(((gvItem.Rows[Convert.ToInt32(e.CommandArgument)].Cells[3].Text.ToString()))));
                    ddDepartment.SelectedIndex = ddDepartment.Items.IndexOf(ddDepartment.Items.FindByText(((gvItem.Rows[Convert.ToInt32(e.CommandArgument)].Cells[4].Text.ToString()))));
                    ddStatus.SelectedIndex = ddStatus.Items.IndexOf(ddStatus.Items.FindByText(((gvItem.Rows[Convert.ToInt32(e.CommandArgument)].Cells[5].Text.ToString()))));
                    txtAuthour.Text = ((gvItem.Rows[Convert.ToInt32(e.CommandArgument)].Cells[7].Text.ToString()));
                    txtPrice.Text = ((System.Web.UI.WebControls.Label)(gvItemDetailForm.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblprice"))).Text.ToString();
                    break;

            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");            
        }
        

    }

    private void PopulateItemDetails()
    {
        try
        {
            objDAL = new LMSDAL.ItemMaintainDal();

            DataView dvItem = new DataView();
            if ((ViewState["sortExpr"]) != null)
            {
                dvItem.Table = objDAL.GetItemMaintainInfo().Tables[0];
                dvItem.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
            }
            else
            {
                dvItem = objDAL.GetItemMaintainInfo().Tables[0].DefaultView;
            }

            gvItemDetailForm.DataSource = dvItem;
            gvItemDetailForm.DataBind();

        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");


         
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    } 

    protected void gvItemDetailForm_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            gvItemDetailForm.PageIndex = e.NewPageIndex;
            PopulateItemDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");  
        }
    } 

    protected void gvItemDetailForm_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            ViewState["sortExpr"] = e.SortExpression;
            if ((String)ViewState["sortDirection"] == "ASC")
            {
                ViewState["sortDirection"] = "DESC";
            }
            else
            {
                ViewState["sortDirection"] = "ASC";
            }
            PopulateItemDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }


    private void PopulateItemtype()
    {
        try
        {
            objDAL = new LMSDAL.ItemMaintainDal();
            ddItemtype.DataSource = objDAL.GetItemTypeDetails().Tables[0];
            ddItemtype.DataTextField = "ItemType";
            ddItemtype.DataValueField = "ItemTypeId";
            ddItemtype.DataBind();
            ddItemtype.Items.Insert(0, new ListItem("--Select Here--", "0"));
            
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objDAL = null;
        }

    }
    private void PopulateStatus()
    {

        try
        {
            objDAL = new LMSDAL.ItemMaintainDal();
            ddStatus.DataSource = objDAL.GetItemStatusDetails().Tables[0];
            ddStatus.DataTextField = "Status";
            ddStatus.DataValueField = "StatusId";
            ddStatus.DataBind();
            ddStatus.Items.Insert(0, new ListItem("--Select Here--", "0"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objDAL = null;
        }



    }
    private void PopulateItemCategory()
    {
        try
        {
            objDAL = new LMSDAL.ItemMaintainDal();
            ddCategory.DataSource = objDAL.GetItemCategoryDetails().Tables[0];
            ddCategory.DataTextField = "Category";
            ddCategory.DataValueField = "CategoryId";
            ddCategory.DataBind();
            ddCategory.Items.Insert(0, new ListItem("--Select Here--", "0"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objDAL = null;
        }

    }
    private void PopoulateDepartment()
    {
        try
        {
            objDAL = new LMSDAL.ItemMaintainDal();
            ddDepartment.DataSource = objDAL.GetItemDepartmentDetails().Tables[0];
            ddDepartment.DataTextField = "Department";
            ddDepartment.DataValueField = "DeptId";
            ddDepartment.DataBind();
            ddDepartment.Items.Insert(0, new ListItem("--Select Here--", "0"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx"); 
           
        }
        finally
        {
            objDAL = null;
        }
    }



    private void Cancel()
    {
        try
        {
            ViewState["ItemId"] = 0;
            ddItemtype.SelectedIndex = ddItemtype.Items.IndexOf(ddItemtype.Items.FindByText("--Select Here--"));
            ddStatus.SelectedIndex = ddStatus.Items.IndexOf(ddStatus.Items.FindByText("--Select Here--"));
            ddCategory.SelectedIndex = ddCategory.Items.IndexOf(ddCategory.Items.FindByText("--Select Here--"));
            ddDepartment.SelectedIndex = ddDepartment.Items.IndexOf(ddDepartment.Items.FindByText("--Select Here--"));
            txtAuthour.Text = "";
            txtPrice.Text = "";
            txtTitle.Text = "";
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }

      
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //int intOutput = 0;
            objInfo = new LMSBLL.ItemMaintainInfo();
            objInfo.itemId = Convert.ToInt32(ViewState["ItemId"]);
            objInfo.ItemTypeId = Convert.ToInt32(ddItemtype.SelectedItem.Value);
            objInfo.title = txtTitle.Text;
            objInfo.category = Convert.ToInt32(ddCategory.SelectedItem.Value);
            objInfo.DeptId = Convert.ToInt32(ddDepartment.SelectedItem.Value);
            objInfo.price = Convert.ToDecimal(txtPrice.Text);
            objInfo.author = txtAuthour.Text;
            objInfo.StatusId = Convert.ToInt32(ddStatus.SelectedValue);
            objDAL = new LMSDAL.ItemMaintainDal();
            int intOutput = objDAL.InsertItemDetails(objInfo);
            PopulateItemDetails();
            switch (intOutput)
            {
                case 1:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Inserted Successfully !');</script>", false);
                    Cancel();
                    break;
                case 2:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Updated Successfully !');</script>", false);
                    Cancel();
                    break;
                case 3:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Null Value Cannot be Accepted !');</script>", false);
                    Cancel();
                    break;
                default:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Error Occured !');</script>", false);
                    Cancel();
                    break;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Cancel();
    }
}










