<%@ Page Language="C#" Theme="LMS" MasterPageFile="~/LMSMaster.master" AutoEventWireup="true"
    CodeFile="ItemStatus.aspx.cs" Inherits="Master_MasterStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="Server">
    <asp:UpdatePanel ID="updtans" runat="server" UpdateMode="Conditional">
<ContentTemplate>
    <table width="100%" cellpadding="1" cellspacing="1">
        <tr class="TRHeader">
            <td align="center" colspan="2">
                <span>Item Status</span>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr> 
       
        <tr>
            <td align="Left" width="100">
                <asp:Label ID="lblStatus" runat="server" CssClass="label" Text="Item Status"></asp:Label>
                <span style="font-weight: bold; color: Red;">*</span>
            </td>
            <td>
               <asp:TextBox ID="txtStatusText" runat="server" MaxLength="50" SkinID="TxtMultiLine"
                            TabIndex="1" ToolTip="Enter Status Text"></asp:TextBox>
                           
            </td>
           
        </tr>
      
        </table>
        
        <table>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr>
            <td align="Center">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="confirmationSave()" TabIndex="10" Text="Save"
                            ToolTip="Click here to save" />
               &nbsp;&nbsp;
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" OnClientClick="confirmationCancel()" TabIndex="11"
                            Text="Cancel" ToolTip="Click here to cancel" />
            </td>
            
        </tr>
        <tr>
        <td>
        </td>
        </tr>
        <tr>
        <td>
        </td>
        </tr>
        </table>
        <table width="100%">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
             <asp:GridView ID="grdViewStatus" runat="server" Width="100%" AutoGenerateColumns="false"
                    AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="silver" HeaderStyle-VerticalAlign="top"
                    OnPageIndexChanging="grdStatus_PageIndexChanging" OnSorting="grdstatus_Sorting" OnRowCommand="grdStatus_RowCommand"
                    BorderColor="#e8c8af" BorderWidth="1px">
                    
                            <Columns>
                                <asp:CommandField ShowSelectButton="True">
                                    <HeaderStyle HorizontalAlign="Center" Width="50%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:CommandField>
                                <asp:BoundField DataField="STATUS" HeaderText="Item Status" SortExpression="STATUS">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" Width="50%" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="StatusId" Visible="False">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatusId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StatusId") %>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
            </td>
        </tr>
    </table>
      <script language="javascript" type="text/javascript">


          function confirmationSave() {

              if (confirm("Are you sure Want to Save?") == true)
                  return true;
              else
                  return false;
          }

          function confirmationCancel() {

              if (confirm("Are you sure Want to Cancel?") == true)
                  return true;
              else
                  return false;
          }
 
    
    </script>
     </ContentTemplate>
        </asp:UpdatePanel> 
</asp:Content>
