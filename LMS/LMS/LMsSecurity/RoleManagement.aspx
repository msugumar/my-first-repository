﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LMSMaster.master" AutoEventWireup="true"
    CodeFile="RoleManagement.aspx.cs" Inherits="Master_security" Theme="LMS" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="Server">
 <asp:UpdatePanel runat="server" ID="updContract">
        <ContentTemplate>
    <table style="width: 100%; font-family: Verdana; font-size: 11px; height: 100%">
        <tr class="TRHeader">
            <td align="Center" colspan="3">
                <span>Role Management</span>
            </td>
        </tr>
        <tr>
        <td></td>
            <td style="width: 44px">
                <asp:Label ID="lblRole" runat="server" Text="Role" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlRole" runat="server" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged"
                    AutoPostBack="True" SkinID="ddlCustomer" ToolTip="Select The Role" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
        <td></td>
            <td style="width: 44px">
                <asp:Label ID="lblRoleTxt" runat="server" Text="Role" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtRole" runat="server" SkinID="TxtMultiLine" ToolTip="Enter New Role Here">
                </asp:TextBox>
            </td>
           
        </tr>
        <tr>
        <td></td>
            <td style="width: 44px">
                <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlStatus" runat="server" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                    SkinID="ddlCustomer" ToolTip="Select The Status">
                </asp:DropDownList>
            </td>
        </tr>
         </table>
         <table style="width: 100%; font-family: Verdana; font-size: 11px; height: 100%">
        <tr>
            <td style="width: 300px" >
                <td style="margin-left: 80px">
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" OnClientClick="return validate();"
                        ToolTip="Click Here To Save" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" OnClientClick="return cancel()"
                        ToolTip="Click Here To Cancel" />
                </td>
            </td>
        </tr>
    </table>
      <table Width="100%">
        <tr>
            <td>
                <asp:GridView ID="PopulateRoleDetails" runat="server" Width="100%" AutoGenerateColumns="false"
                    BorderStyle="Groove" AllowPaging="true" AllowSorting="true" HeaderStyle-VerticalAlign="Top"
                    OnSorting="PopulateRoleDetails_sorting" OnPageIndexChanging="PopulateRoleDetails_PageIndexChanging"
                    OnSelectedIndexChanged="PopulateRoleDetails_SelectedIndexChanged" BorderColor="#e8c8af"
                    BorderWidth="1px">
                    <Columns>
                        <asp:BoundField DataField="ROLE" HeaderText="Role Name" SortExpression="ROLE">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle Width="1%" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle Width="1%" HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle VerticalAlign="Top" BackColor="Silver"></HeaderStyle>
                </asp:GridView>
            </td>
        </tr>
        </tr>
    </table>
    </ContentTemplate>
</asp:UpdatePanel>




    <script language="javascript" type="text/javascript">
        function validate() {

            var varrole = document.getElementById("<%=ddlRole.ClientID %>");

            if (varrole == null) {
                if (document.getElementById("<%=txtRole.ClientID%>").value == "") {
                    alert("Please Enter The Role Type To Proceed Further");
                    document.getElementById("<%=txtRole.ClientID%>").focus();
                    return false;
                }
                if (document.getElementById("<%=ddlStatus.ClientID%>").selectedIndex == "") {
                    alert("Please Select The Status To Proceed Further");
                    document.getElementById("<%=ddlStatus.ClientID%>").focus();
                    return false;
                }
                var a;

                a = confirm("Do you really want To Add the New Role ?");
                if (!a) {
                    return false
                }
            }
            else {

                if (document.getElementById("<%=ddlRole.ClientID%>").selectedIndex == "") {
                    alert("Please Select The Role  To Proceed Further");
                    document.getElementById("<%=ddlRole.ClientID%>").focus();
                    return false;

                }
                if (document.getElementById("<%=ddlStatus.ClientID%>").selectedIndex == "") {
                    alert("Please Select The Status To Proceed Further");
                    document.getElementById("<%=ddlStatus.ClientID%>").focus();
                    return false;
                }
                var a;
                a = confirm("Do you really want To Change the Status ?");
                if (!a) {
                    return false
                }
            }
        }
        function cancel() {
            var a;
            a = confirm("Do you really want to Cancel ?");
            if (!a) {
                return false;
            }
         }
        function spacevalidate(e) {
            var KeyID = (window.event) ? event.keyCode : e.which;
            if ((KeyID == 32 || (KeyID >= 95 && KeyID <= 105) ||(KeyID >=48  && KeyID <= 57) )) {
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    </asp:Content>

  