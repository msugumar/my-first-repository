﻿<%@ Page Title="" Language="C#" Theme="LMS" MasterPageFile="~/LMSMaster.master" AutoEventWireup="true"
    CodeFile="RolePrivilages.aspx.cs" Inherits="LMsSecurity_Roles"%>

<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="Server">

    <script language="javascript" type="text/javascript">
    function validate() {
        if (document.getElementById("<%=ddlRole.ClientID%>").value == "Select Here") {
            alert("Please Select The  Role Its Empty Now");
            document.getElementById("<%=ddlRole.ClientID%>").focus();
            return false;
        }        
        var a;
        a = confirm("Do you really want Assign These Forms ?");
        if (!a) {
            return false;
        }
    }
    function cancel() {
        var a;
        a = confirm("Do you really want to Cancel ?");
        if (!a) {
            return false;
        }
    }   
</script>
 <asp:UpdatePanel runat="server" ID="updContract">
        <ContentTemplate>
    <table style="width: 100%; font-family: Verdana; font-size: 11px; height: 100%">
        <tr class="TRHeader">
            <td align="Center" colspan="3">
                <span>Role Privilage </span>
            </td>
        </tr>
        <tr><td /></tr>
        <tr>
        <td></td>
        <td>
            <asp:Label ID="lblRole" runat="server" Text="Role"></asp:Label></td>
            <td>
                <asp:DropDownList ID="ddlRole" runat="server" onselectedindexchanged="ddlPopulateRole_SelectedIndexChanged" 
                    AutoPostBack="true" SkinID="ddlCustomer" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr><td /></tr>
        </table>
         <table style="width: 100%; font-family: Verdana; font-size: 11px; height: 100%">
        <tr>            
            <td>
                <asp:GridView ID="gvFormsDetails" runat="server" AllowPaging="true" 
                    AllowSorting="true" AutoGenerateColumns="false" 
                    HeaderStyle-BackColor="Chocolate" HeaderStyle-VerticalAlign="Top" 
                    Height="100px" OnCheckedChanged="gvform_CheckedChanged" 
                    OnPageIndexChanging="gvFormsDetails_PageIndexChanging" 
                    onrowcommand="gvFormsDetails_RowCommand1" OnSorting="gvformdetails_sorting" 
                    ToolTip="Form Assignment" Width="750px">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <HeaderTemplate>
                                Read
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkRead" runat="server" autopostback="true" 
                                    OnCheckedChanged="chkRead_CheckedChanged"  ToolTip="To Give Read Access Click here" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <HeaderTemplate>
                                Write
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkwrite" runat="server" autopostback="true" 
                                    OnCheckedChanged="chkWrite_CheckedChanged" ToolTip="To Give Read Access Click Here" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FormName" HeaderText="FormsName" 
                            SortExpression="FormName">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FormDescription" HeaderText="Description" 
                            SortExpression="FormDescription">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField Visible="false">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <HeaderTemplate>
                                FormID
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFormId" runat="server" 
                                    Text='<%# DataBinder.Eval (Container.DataItem, "FormID") %> '></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="Silver" VerticalAlign="Top" />
                </asp:GridView>
            </td>
        </tr>
                
    </table>
    <table style="width: 100%; font-family: Verdana; font-size: 11px; height: 100%">
    <tr><td>
    <td></td>
    <td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><td><asp:Button ID="btnSave" runat="server" Text="Save" 
onclick="btnSave_Click" />
                
                   
                
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" OnClientClick="return cancel()" /></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td></td>
    </td></tr>
    </table>
    </ContentTemplate>
     </asp:UpdatePanel>
        
</asp:Content>
