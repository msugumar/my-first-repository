﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using LMSBLL;
using LMSDAL;
using ExceptionalHandling;
using AjaxControlToolkit;



public partial class LMsSecurity_Roles : System.Web.UI.Page
{
    public string strErrorMessage;
    public string strPage;
    public string[] strResult;
    public String strRole = string.Empty;
    public LMSBLL.RolesInfo objInfo;
    public LMSDAL.RoleDal objDAL;

    #region "Page Load....."

    protected void Page_Load(object sender, EventArgs e)
    {
     
      strRole = Request.QueryString["Role"];
        try
        {
           
            if (!IsPostBack)
            {
                AccessIdentifier();

                PopulateFormGrid();
                populateFormRoles();

                if (strRole != null)
                {

                    ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByText(strRole));
                }
            }
           btnSave.Attributes.Add("OnClick", "return validate()");
        }
           
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
    }

    #endregion "Page Load......."

    #region "Populate Roles To drop down View........"

    private void populateFormRoles()
    {
        try
        {
            objDAL = new LMSDAL.RoleDal();
            ddlRole.DataSource = objDAL.PopulateRoles().Tables[0];
            ddlRole.DataTextField = "role";
            ddlRole.DataValueField = "roleid";
            ddlRole.DataBind();
            ddlRole.Items.Insert(0, "Select Here");
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }

    } 

    protected void ddlPopulateRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int intFormID = 0;
            string strReadAccessString = string.Empty;
            string strWriteAccessString = string.Empty;
            objDAL = new LMSDAL.RoleDal();
            objInfo = new LMSBLL.RolesInfo();
            DataSet dsForm = new DataSet();

            objInfo.roleid = Convert.ToInt32(ddlRole.SelectedItem.Value);

            dsForm = objDAL.RetriveValues(objInfo);

            if (dsForm.Tables.Count > 0)
            {
                if (dsForm.Tables[0].Rows.Count > 0)
                {
                    for (int icount = 0; icount < gvFormsDetails.Rows.Count; icount++)
                    {
                        intFormID = Convert.ToInt32(((Label)(gvFormsDetails.Rows[icount].FindControl("lblFormId"))).Text);

                        foreach (DataRow dr in dsForm.Tables[0].Rows)
                        {
                            if (Convert.ToInt32(dr["FormId"]) == intFormID)
                            {
                                CheckBox cread = ((CheckBox)gvFormsDetails.Rows[icount].FindControl("chkRead"));
                                if (Convert.ToBoolean(dr["bitReadAccess"]) == true)
                                {
                                    cread.Checked = true;
                                }
                                else
                                {
                                    cread.Checked = false;
                                }
                                CheckBox cwrite = ((CheckBox)gvFormsDetails.Rows[icount].FindControl("chkWrite"));
                                if (Convert.ToBoolean(dr["bitWriteAccess"]) == true)
                                {
                                    cwrite.Checked = true;
                                }
                                else
                                {
                                    cwrite.Checked = false;
                                }
                                //PopulateFormGrid();

                            }
                        }
                    }
                }
                else
                {
                    ClearGrid();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    #endregion "Populate Roles To DropDown Ends"

    #region "Check Changed Event"
    protected void chkRead_CheckedChanged(object source, EventArgs e)
    {
        try
        {
            for (int intloop = 0; gvFormsDetails.Rows.Count > intloop; intloop++)
            {
                CheckBox CWrite = ((CheckBox)(gvFormsDetails.Rows[Convert.ToInt32(intloop)].FindControl("chkwrite")));
                CheckBox CRead = ((CheckBox)(gvFormsDetails.Rows[Convert.ToInt32(intloop)].FindControl("chkRead")));
                if (CRead.Checked == false)
                {
                    CWrite.Checked = false;
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");


        }
    }
    protected void chkWrite_CheckedChanged(object source, EventArgs e)
    {
        try
        {
            for (int intloop = 0; gvFormsDetails.Rows.Count > intloop; intloop++)
            {
                CheckBox CWrite = ((CheckBox)(gvFormsDetails.Rows[Convert.ToInt32(intloop)].FindControl("chkwrite")));
                CheckBox CRead = ((CheckBox)(gvFormsDetails.Rows[Convert.ToInt32(intloop)].FindControl("chkRead")));
                if (CWrite.Checked == true)
                {
                    CRead.Checked = true;
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
    }
    #endregion

    #region "Populate Roles TO Grid View........"

    private void PopulateFormGrid()
    {
        try
        {
            objDAL = new LMSDAL.RoleDal();
            DataView dvFormView = new DataView();
            if ((ViewState["sortExpr"]) != null)
            {
                dvFormView.Table = objDAL.PopulateForms().Tables[0];
                dvFormView.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
            }
            else
            {
                dvFormView = objDAL.PopulateForms().Tables[0].DefaultView;
            }

            gvFormsDetails.DataSource = dvFormView;
            gvFormsDetails.DataBind();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    protected void gvformdetails_sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            ViewState["sortExpr"] = e.SortExpression;
            if ((String)ViewState["sortDirection"] == "ASC")
            {
                ViewState["sortDirection"] = "DESC";
            }
            else
            {
                ViewState["sortDirection"] = "ASC";
            }
            PopulateFormGrid();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
    }

    protected void gvFormsDetails_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            gvFormsDetails.PageIndex = e.NewPageIndex;
            PopulateFormGrid();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
    }

   
    protected void gvFormsDetails_RowCommand1(object sender, GridViewCommandEventArgs e)
    {

    }

    #endregion "POpulate Roles TO grid View Ends....."

    #region "Save Roles TO DataBAse....."

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string strFormIDString = string.Empty;
            string strReadAccessString = string.Empty;
            string strWriteAccessString = string.Empty;
            Label lblErrorMessage = new Label();
            Collection<string> items = new Collection<string>();
            for (int intCount = 0; gvFormsDetails.Rows.Count > intCount; intCount++)
            {
                objInfo = new LMSBLL.RolesInfo();
                objDAL = new LMSDAL.RoleDal();
                strFormIDString = strFormIDString + Convert.ToString(((Label)(gvFormsDetails.Rows[Convert.ToInt32(intCount)].FindControl("lblFormId"))).Text.ToString()) + "ì";
                if (((CheckBox)gvFormsDetails.Rows[intCount].FindControl("chkRead")).Checked)
                {
                    strReadAccessString = strReadAccessString + "1" + "ì";
                }
                else
                {
                    strReadAccessString = strReadAccessString + "0" + "ì";
                }

                if (((CheckBox)gvFormsDetails.Rows[intCount].FindControl("chkWrite")).Checked)
                {
                    strWriteAccessString = strWriteAccessString + "1" + "ì";
                }
                else
                {
                    strWriteAccessString = strWriteAccessString + "0" + "ì";

                }
            }
            objInfo.roleid = Convert.ToInt32(ddlRole.SelectedValue);
            objInfo.FormId = strFormIDString;
            objInfo.bitReadAccess = strReadAccessString;
            objInfo.bitWriteAccess = strWriteAccessString;

            int output = objDAL.SaveRoleAssignment(objInfo);

            switch (output)
            {
                case 0:
                    lblErrorMessage.Text = "The error has occured";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The error has occured.');</script>", false);
                    Clear();
                    break;
                case 1:
                    lblErrorMessage.Text = "Inserted Successfully";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Item Type details Inserted successfully');</script>", false);
                    Clear();
                    break;
                case 2:
                    lblErrorMessage.Text = "Updated Sucessfully";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Roles Updated Sucessfully');</script>", false);
                    Clear();
                    break;
            }

        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
    }

    #endregion "Save Roles TO DataBAse....."

    #region "Default Value Setting"

    private void Clear()
    {
        try
        {
            ddlRole.SelectedIndex = 0;
            ClearGrid();
            
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");


        }
    }

    private void ClearGrid()
    {
        try
        {
            for (int intloop = 0; gvFormsDetails.Rows.Count > intloop; intloop++)
            {
                CheckBox CWrite = ((CheckBox)(gvFormsDetails.Rows[Convert.ToInt32(intloop)].FindControl("chkwrite")));
                CheckBox CRead = ((CheckBox)(gvFormsDetails.Rows[Convert.ToInt32(intloop)].FindControl("chkRead")));
                CWrite.Checked = false;
                CRead.Checked = false;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");


        }
    }


    #endregion

    #region "Cancel"

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();
    }

    #endregion

    #region "Security"
    private void AccessIdentifier()
    {
        try
        {
            objInfo = new LMSBLL.RolesInfo();
            objDAL = new LMSDAL.RoleDal();
            //objDAL = new LMSDAL.ItemMaintainDal();
            strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
            WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            objInfo.LoginId = ExtractUserName(struser.Identity.Name);
            int intLen = 0;
            intLen = strPage.IndexOf(".");
            objInfo.FormName = strPage.Substring(0, intLen);
            strResult = objDAL.Accessverifyer(objInfo);

            if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
            {
                btnSave.Enabled = true;
            }
            else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
            {
                btnSave.Enabled = false;
            }
            else
            {
                AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane1");
                LinkButton lnkUserMagt = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkUserMagt");
                Response.Redirect("~/UnAuthorization.aspx?file=" + lnkUserMagt.Text.ToString(), false);
                return;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    public string ExtractUserName(string path)
    {
        /*  try
          {*/
        string[] userPath = path.Split(new char[] { '\\' });

        return userPath[userPath.Length - 1];
        /*      }
              catch (Exception ex)
         * 
              {
                  ErrorHandler.RaiseError(ex, strLogError);
              } */
    }

    #endregion 

  
}
   


