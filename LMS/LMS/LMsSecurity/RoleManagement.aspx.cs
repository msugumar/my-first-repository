﻿#region "NameSpaces"
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using LMSBLL;
using LMSDAL;
using AjaxControlToolkit;
using ExceptionalHandling;

#endregion

public partial class Master_security : System.Web.UI.Page
{
        #region "Declaration"
    public string strErrorMessage;
    public string strPage;
    public string[] strResult;
    public LMSBLL.RolesInfo objInfo;
    public LMSDAL.RoleDal objDAL;
    #endregion

         #region "PageLoad......"
    protected void Page_Load(object sender, EventArgs e)
    {
        string strTest = ddlRole.SelectedValue;
        try
        {
          //  txtRole.Attributes.Add("onKeyDown", "return spacevalidate(event)");
            if (!IsPostBack)
            {
                AccessIdentifier();
                PopulateStatus();
                PopulateRoles();
                PopulateRoleView();
                txtRole.Visible = false;
                lblRoleTxt.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();

            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

            Session["ErrorID"] = intError.ToString();

            Response.Redirect("CustomError.aspx");

        }
    }
    #endregion "PageLoad....."

         #region "Populate Status To DropDown....."

        private void PopulateStatus()
        {
            try
            {
                objDAL = new LMSDAL.RoleDal();
                DropDownList ddlGetStatus = new DropDownList();
                ddlStatus.DataSource = objDAL.PopulateStatus().Tables[0];
                ddlStatus.DataTextField = "Status";
                ddlStatus.DataValueField = "StatusId";
                ddlStatus.DataBind();
                ddlStatus.Items.Insert(0, "Select Here");
            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
            finally
            {
                objDAL = null;
                objInfo = null;
                    
            }

        }

        #endregion "Populate Status To DropDown....."

         #region "Populate Roles TO GridView....."

        private void PopulateRoleView()
        {
            try
            {
                objDAL = new LMSDAL.RoleDal();
                DataView dvRoleView = new DataView();
                if ((ViewState["sortExpr"]) != null)
                {
                    dvRoleView.Table = objDAL.PopulateRoleGrid().Tables[0];
                    dvRoleView.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
                }
                else
                {
                    dvRoleView = objDAL.PopulateRoleGrid().Tables[0].DefaultView;
                }
                PopulateRoleDetails.DataSource = dvRoleView;
                PopulateRoleDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
            finally
            {
                objInfo = null;
                objDAL = null;
            }

        }
        protected void PopulateRoleDetails_sorting(object source, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            try
            {
                ViewState["sortExpr"] = e.SortExpression;
                if ((String)ViewState["sortDirection"] == "ASC")
                {
                    ViewState["sortDirection"] = "DESC";
                }
                else
                {
                    ViewState["sortDirection"] = "ASC";
                }
                PopulateRoleView();
            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
        }
        protected void PopulateRoleDetails_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            try
            {
                PopulateRoleDetails.PageIndex = e.NewPageIndex;
                PopulateRoleView();
            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
        }
        #endregion "Populate Roles TO GridView....."

         #region "Populate Roles To DropDown....."
        private void PopulateRoles()
        {
            try
            {
                objDAL = new LMSDAL.RoleDal();
                DropDownList ddlGetRoles = new DropDownList();
                ddlRole.DataSource = objDAL.PopulateRoles().Tables[0];
                ddlRole.DataTextField = "role";
                ddlRole.DataValueField = "roleid";
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, "Select Here");
                ddlRole.Items.Insert(1, "--AddNew--");
            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
        }
        
        #endregion ""Populate Roles To GridView.....""

         #region "Selected Index....."
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet dsResult=new DataSet();
            objDAL = new LMSDAL.RoleDal();
            objInfo = new LMSBLL.RolesInfo();
            try
            {
                if (ddlRole.SelectedValue == "--AddNew--")
                {
                    lblRoleTxt.Visible = true;
                    txtRole.Visible = true;
                    lblRole.Visible = false;
                    ddlRole.Visible = false;
                }
                else
                {

                    objInfo.roleid = Convert.ToInt32(ddlRole.SelectedValue);
                    dsResult = objDAL.RetriveRoleStatus(objInfo);
                    ddlStatus.SelectedValue = dsResult.Tables[0].Rows[0]["StatusId"].ToString();
                }


            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
            finally
            {
                objInfo = null;
                objDAL = null;
            }

        }
        protected void txtRole_TextChanged(object sender, EventArgs e)
        {
        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void PopulateRoleDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateRoleView();
        }
        #endregion ""Drop Down  Selected Index.....""

         #region "Cancel....."
        protected void btncancel_Click(object sender, EventArgs e)
        {
            Drop();
        }
        #endregion "cancel....."

         #region "Save....."

        protected void btnSave_Click(object sender, EventArgs e)
        {
                save();
           
        }

        private void save()
        {
            try
            {
                objInfo = new LMSBLL.RolesInfo();

                if (txtRole.Text.ToString() == "")
                {
                    objInfo.rolename = ddlRole.SelectedItem.Text;
                }
                else
                {
                    objInfo.rolename = txtRole.Text;
                }

                objInfo.statusinfo = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                objDAL = new LMSDAL.RoleDal();
                int output = objDAL.SaveRoles(objInfo);
                PopulateRoleView();
                PopulateRoles();
                Label lblErrorMessage = new Label();

                switch (output)
                {
                    case 0:
                        lblErrorMessage.Text = "The error has occured";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The error has occured.');</script>", false);
                        txtRole.Text = "";
                        Drop();
                        break;
                    case 1:
                        lblErrorMessage.Text = "Inserted Successfully";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The New Role Added Sucessfully And It navigate to RolePrivilage Page');</script>", false);
                        Drop();                
                        
                        //Response.Redirect("Roles.aspx? Role={0} " + txtRole.Text);
                        break;
                    case 2:
                        lblErrorMessage.Text = "Updated Sucessfully";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Role Updated Sucessfully');</script>", false);
                        Drop();
                        break;
                    case 3:
                   
                        lblErrorMessage.Text = "NUll VAlues Cannot Be Inserted";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Null Role Cannot Be Inserted');</script>", false);
                        Drop();
                        break;
                }

            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
            finally
            {
                //Response.End();
                objDAL = null;
                objInfo = null;
               
            }

        }
         #endregion 
  
         #region "Security"
        private void AccessIdentifier()
        {
            try
            {
                objInfo = new LMSBLL.RolesInfo();
                objDAL = new LMSDAL.RoleDal();
                //objDAL = new LMSDAL.ItemMaintainDal();
                strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
                WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
                objInfo.LoginId = ExtractUserName(struser.Identity.Name);
                int intLen = 0;
                intLen = strPage.IndexOf(".");
                objInfo.FormName = strPage.Substring(0, intLen);
                strResult = objDAL.Accessverifyer(objInfo);

                if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
                {
                    btnSave.Enabled = true;
                }
                else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
                {
                    btnSave.Enabled = false;
                }
                else
                {
                    AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane1");
                    LinkButton lnkRoleManagt = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkRoleManagt");
                    Response.Redirect("~/UnAuthorization.aspx?file=" + lnkRoleManagt.Text.ToString(), false);
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
            finally
            {
                objInfo = null;
                objDAL = null;
            }
        }

        public string ExtractUserName(string path)
        {
            /*  try
              {*/
            string[] userPath = path.Split(new char[] { '\\' });

            return userPath[userPath.Length - 1];
            /*      }
                  catch (Exception ex)
             * 
                  {
                      ErrorHandler.RaiseError(ex, strLogError);
                  } */
        }
        #endregion

         #region "Default"
        private void Drop()
        {
            try
            {
                lblRoleTxt.Visible = false;
                txtRole.Visible = false;
                lblRole.Visible = true;
                ddlRole.Visible = true;
                ddlRole.SelectedIndex = 0;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();

                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);

                Session["ErrorID"] = intError.ToString();

                Response.Redirect("CustomError.aspx");

            }
        }
        #endregion


}

