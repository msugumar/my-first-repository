<%@ Page Title="User Management" Theme="LMS" Language="C#" MasterPageFile="~/LMSMaster.master"
    AutoEventWireup="true" CodeFile="UserManagement.aspx.cs" Inherits="LMsSecurity_UserManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>

            <script language="javascript" type="text/javascript">
                function OpenUserRoleDialog(url, btnName, diaHeight) {
                    var vReturnValue;
                    if (diaHeight == null || diaHeight == "")
                        diaHeight = "324";

                    if (url != null) {
                        vReturnValue = window.showModalDialog(url, "myobject", "dialogHeight: " + diaHeight + "px; dialogTop: 350px; dialogLeft: 211px; dialogWidth:800px; center: Yes; help: No; resizable: No; status: No; title:asd");
                        //vReturnValue = window.showModalDialog(url, "#1", "dialogHeight: " + diaHeight + "px; dialogWidth: 600px; dialogTop: 190px; dialogLeft: 220px; edge: Raised; center: Yes; help: No; resizable: No; status: No;");
                    }
                    else {
                        alert("No URL passed to open");
                    }
                    if (vReturnValue != null && vReturnValue == true) {
                        return vReturnValue
                    }
                    else {
                        return false;
                    }
                }
                function OpenRolePrivilageDialog(url) {
                    newwindow = window.open(url, 'name', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
                    if (window.focus) {
                        newwindow.focus()
                    }
                    return false;

                }

                function Validation() {

                    if (document.getElementById("<%= ddUserName.ClientID %>").selectedIndex == "") {
                        alert("Please Select the User !")
                        document.getElementById("<%= ddUserName.ClientID %>").focus();
                        return false;
                    }
                    if (document.getElementById("<%=ddUserRole.ClientID%>").selectedIndex == "") {
                        alert("Please Select the User Role. \n  ");
                        document.getElementById("<%=ddUserRole.ClientID%>").focus();
                        return false;
                    }
                    // Validation for Save the User Role 
                    var a;
                    a = confirm("Do you really want Assign the Selected Role ?");
                    if (!a) {
                        return false
                    }
                }
            </script>

            <table style="width: 100%; font-family: Verdana; font-size: 11px; height: 100%">
                <tr class="TRHeader">
                    <td align="center" colspan="4">
                        <span>User Management</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 94px">
                        <asp:Label ID="lblUserName" CssClass="label" runat="server" Text="User" ToolTip="User Name"></asp:Label>
                    </td>
                    <td style="width: 409px; margin-left: 120px">
                        <asp:DropDownList ID="ddUserName" runat="server" AutoPostBack="true" TabIndex="1"
                            ToolTip="Select The User Name" OnSelectedIndexChanged="ddUserName_SelectedIndexChanged"
                            SkinID="ddlCustomer">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 113px">
                        <asp:Label ID="lblUserRole" CssClass="label" runat="server" Text="Role" ToolTip="User Role"></asp:Label>
                    </td>
                    <td style="width: 553px; margin-left: 160px;">
                        <asp:DropDownList ID="ddUserRole" runat="server" TabIndex="2" SkinID="ddlCustomer" OnSelectedIndexChanged="ddUserRole_SelectedIndexChanged">
                        </asp:DropDownList>
                        <%--<asp:DropDownList ID="ddUserRole" runat="server" Width="148px" ToolTip="Select the User Role"
                    SkinID="ddlCustomer" 
                    onselectedindexchanged="ddUserRole_SelectedIndexChanged">
                </asp:DropDownList>--%>
                        <asp:LinkButton ID="lnkRoleView" runat="server" TabIndex="3" ToolTip="Link to Show the Role Privilage" >Display Role Privileges</asp:LinkButton>
                        <%-- OnClick="lnkRoleView_Click"--%>
                        <%--OnClientClick="javascript:OpenRolePrivilageDialog(); return false;"--%>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblEmail" CssClass="label" runat="server" Text="Email"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblDispEmail" CssClass="label" runat="server" Text="" ToolTip="Email Id for the Selected User"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblLoginID" CssClass="label" runat="server" Text="Login ID"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblDispLoginID" CssClass="label" runat="server" Text="" ToolTip="Login Id for the Selected User"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="left">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="butSave" runat="server" Text="Save" TabIndex="4" ToolTip="Click here to Save"
                            OnClick="butSave_Click" />
                        &nbsp;&nbsp;
                        <asp:Button ID="butCancel" runat="server" Text="Cancel" TabIndex="5" ToolTip="Click here to Cancel"
                            OnClick="butCancel_Click" />
                    </td>
                </tr>
            </table>
            <%--Confirm Button Code--%>
            <%-- <cc1:ConfirmButtonExtender runat="server" ID="cbeSave" TargetControlID="butSave"
        ConfirmText="Are you sure you want to Save the Role ?" Enabled="true" />--%>
            <%--Code for Confirmation--%>
            <%--<cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="popup"
        TargetControlID="butSave" PopupControlID="pnlPopup" BackgroundCssClass="modalBackground"
        OkControlID="butOk" />
    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalpopup" Style="display: none">
        <div class="container">
            <div class="header">
                <asp:Label ID="Label1" runat="server" CssClass="msg" Text="Are you sure?" />
                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="close" OnClientClick="$find('popup').hide(); return false;" />
            </div>
            <div class="body">
                <asp:Label ID="Label2" runat="server" CssClass="msg" Text="Do you want to continue?" />
            </div>
            <div class="footer">
                <asp:Button ID="butOk" runat="server" Text="Yes" Width="40px" OnClick="butSave_Click" />
                <asp:Button ID="butPopupCancel" runat="server" Text="No" Width="40px" OnClientClick="$find('popup').hide(); return false;" />
            </div>
        </div>
    </asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
