﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LMsSecurity_RoleUC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
     {
        DropDownList drpRole = (DropDownList)Spinner1.FindControl("ddlRole");
        GridView gvDetails = (GridView)Spinner1.FindControl("gvFormsDetails");
        gvDetails.Visible = true;
        Button btnSave = (Button)Spinner1.FindControl("btnSave");
        Button btnCanel = (Button)Spinner1.FindControl("btnCancel");
        btnSave.Visible = false;
        btnCanel.Visible = false;
    }
}
