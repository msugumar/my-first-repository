﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using LMSBLL;
using LMSDAL;
using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using AjaxControlToolkit;
using ExceptionalHandling;

public partial class LMsSecurity_UserManagement : System.Web.UI.Page
{
    public LMSBLL.UserManagementInfo objInfo;
    public LMSDAL.UserManagementDAL objDAL;
    public string strPage;
    public string[] strResult;
    public string strErrorMessage;


    #region "Page Load"
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
               // AccessIdentifier();
                PopulateUserNameDropDown();
                PopulateUserRoleDropDown();
                lnkRoleView.Visible = false;
            }
            butSave.Attributes.Add("OnClick", "return Validation()");
            if (ddUserRole.SelectedValue != "--Select Here--")
            {
            string ScriptOpenRolePrivilageDialog = "javascript:OpenUserRoleDialog('{0}','{1}');";
            string strURL = "../LMsSecurity/RoleAssaign.aspx?Role=" + ddUserRole.SelectedValue;
            lnkRoleView.Attributes.Add("OnClick", string.Format(ScriptOpenRolePrivilageDialog, strURL, "350"));
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            objErrorHandler.RaiseError(ex, strErrorMessage);

        }

        
        
    }
    #endregion 

    #region " Populate UserName "
    public void PopulateUserNameDropDown()
    {
        try
        {
            objDAL = new LMSDAL.UserManagementDAL();
            ddUserName.DataSource = objDAL.GetUserNameInfo().Tables[0];
            ddUserName.DataTextField = "UserName";
            ddUserName.DataValueField = "UserId";
            ViewState["UserId"] = ddUserName.SelectedValue;

            ddUserName.DataBind();
            ddUserName.Items.Insert(0, "--Select Here--");
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            objErrorHandler.RaiseError(ex, strErrorMessage);
        }
    }
    #endregion

    #region " Populate User Roles"
    public void PopulateUserRoleDropDown()
    {
        try
        {
            objDAL = new LMSDAL.UserManagementDAL();
            ddUserRole.DataSource = objDAL.GetUserRoleInfo().Tables[0];
            ddUserRole.DataTextField = "Role";
            ddUserRole.DataValueField = "RoleId";
            ViewState["RoleId"] = ddUserRole.SelectedValue;

            ddUserRole.DataBind();
            ddUserRole.Items.Insert(0, "--Select Here--");
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            objErrorHandler.RaiseError(ex, strErrorMessage);
        }
    }
    #endregion

    #region "Select The Role Details with User Name in Dropdown"
    protected void ddUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dvwUserEmailLoginInfo = new  DataSet();
        try
        {
            if (ddUserName.SelectedItem.Value == "--Select Here--")
            {
                ddUserName.ClearSelection();
                ddUserRole.ClearSelection();
                lblDispLoginID.Text = "";
                lblDispEmail.Text = "";
                lnkRoleView.Visible = false;
            }
            else
            {
                objInfo = new LMSBLL.UserManagementInfo();
                objDAL = new LMSDAL.UserManagementDAL();
                objInfo.UserID = ddUserName.SelectedValue;
                dvwUserEmailLoginInfo = objDAL.GetUserEmailandLoginInfo(objInfo);
                if (dvwUserEmailLoginInfo.Tables[0].Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(dvwUserEmailLoginInfo.Tables[0].Rows[0]["RoleId"])) == false)
                    {
                        lblDispEmail.Text = dvwUserEmailLoginInfo.Tables[0].Rows[0]["EmailId"].ToString();
                        lblDispLoginID.Text = dvwUserEmailLoginInfo.Tables[0].Rows[0]["LoginId"].ToString();
                        ddUserRole.SelectedValue = dvwUserEmailLoginInfo.Tables[0].Rows[0]["RoleId"].ToString();
                        lnkRoleView.Visible = true;
                    }
                    else
                    {
                        lblDispEmail.Text = dvwUserEmailLoginInfo.Tables[0].Rows[0]["EmailId"].ToString();
                        lblDispLoginID.Text = dvwUserEmailLoginInfo.Tables[0].Rows[0]["LoginId"].ToString();
                        lnkRoleView.Visible = false;
                        ddUserRole.ClearSelection();
                    }
                }
            }
         }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
            dvwUserEmailLoginInfo.Dispose();
        }
    }
    #endregion

    #region "Methods for Save the User Roles"

    protected void butCancel_Click(object sender, EventArgs e)
    {
        ddUserName.ClearSelection();
        ddUserRole.ClearSelection();
        lblDispLoginID.Text = "";
        lblDispEmail.Text = "";
        lnkRoleView.Visible = false;
    }

    public void butSave_Click(object sender, EventArgs e)
    {
        try
        {
            SaveUserRole();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            objErrorHandler.RaiseError(ex, strErrorMessage);
        }

    }
  
    protected void SaveUserRole()
    {
        try
        {
            objInfo = new LMSBLL.UserManagementInfo();
            objInfo.UserID = ddUserName.SelectedValue;
            objInfo.RoleID = ddUserRole.SelectedValue;
            objDAL = new LMSDAL.UserManagementDAL();
            int intOutput = objDAL.InsertUserRole(objInfo);
            switch (intOutput)
            {
                case 1:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The New Role is Inserted successfully');</script>", false);
                    lnkRoleView.Visible = false;
                    break;
                case 2:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Role is Updated successfully');</script>", false);
                    lnkRoleView.Visible = false;
                    break;
                //default:
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Select the Field');</script>", false);
                //    break;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
            ddUserName.ClearSelection();
            ddUserRole.ClearSelection();
            lblDispLoginID.Text = "";
            lblDispEmail.Text = "";
        }
    }

    #endregion

    #region "PopUp Methods"
    protected void butOk_Click(object sender, EventArgs e)
    {
       butSave_Click(null, null);
    }
    protected void butOK_Click(object sender, EventArgs e)
    {
        try
        {
         objInfo = new LMSBLL.UserManagementInfo();
            objInfo.UserID = ddUserName.SelectedValue;
            objInfo.RoleID = ddUserRole.SelectedValue;
            objDAL = new LMSDAL.UserManagementDAL();
            int intOutput = objDAL.InsertUserRole(objInfo);
            switch (intOutput)
            {
                case 1:
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The New Role is Inserted successfully');</script>", false);
                    break;
                case 2:
                    // lblErrorMessage.Text = "Updated Successfully";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Role is Updated successfully');</script>", false);
                    break;
                //default:
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Select the Field');</script>", false);
                //    break;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
            ddUserName.ClearSelection();
            ddUserRole.ClearSelection();
            lblDispLoginID.Text = "";
            lblDispEmail.Text = "";
        }
    }
    #endregion

    #region "Page Access"
    private void AccessIdentifier()
    {
        try
        {
            objInfo = new LMSBLL.UserManagementInfo();
            objDAL = new LMSDAL.UserManagementDAL();
            strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
            WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            objInfo.LoginId = ExtractUserName(struser.Identity.Name);
            int intLen = 0;
            intLen = strPage.IndexOf(".");
            objInfo.FormName = strPage.Substring(0, intLen);
            strResult = objDAL.Accessverifyer(objInfo);

            if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
            {
                butSave.Enabled = true;
            }
            else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
            {
                butSave.Enabled = false;
            }
            else
            {
                AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane1");
                LinkButton lnkItemMaintain = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkRoleMaintenance");
                Response.Redirect("~/UnAuthorization.aspx?file=" + lnkItemMaintain.Text.ToString(), false);
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    public string ExtractUserName(string path)
    {
        string[] userPath = path.Split(new char[] { '\\' });
        return userPath[userPath.Length - 1];
    }
    #endregion

  
    protected void ddUserRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddUserRole.SelectedItem.Value != "--Select Here--")
        {
            lnkRoleView.Visible = true;
        }
        else
        {
            lnkRoleView.Visible = false;
        }
    }
    
    #region "Method For Display the PopUp Window"
    protected void lnkRoleView_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddUserRole.SelectedValue != "--Select Here--")
            {
                //string strURL = "../LMsSecurity/RoleAssaign.aspx?Role=" + ddUserRole.SelectedValue;
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", strURL, true);

                string ScriptOpenRolePrivilageDialog = "javascript:OpenUserRoleDialog('{0}','{1}');";
                string strURL = "../LMsSecurity/RoleAssaign.aspx?Role=" + ddUserRole.SelectedValue;
               // Response.Redirect(String.Format("RolePrevilage.aspx?Role={0}", ddUserRole.SelectedIndex));
                lnkRoleView.Attributes.Add("OnClick", string.Format(ScriptOpenRolePrivilageDialog, strURL, "350"));
              
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Select the Role');</script>", false);
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }
    #endregion
}
