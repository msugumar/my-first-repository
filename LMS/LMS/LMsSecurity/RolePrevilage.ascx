﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RolePrevilage.ascx.cs"
    ClassName="Spinner" Inherits="LMsSecurity_RolePrevilage_Copy" %>
<style type="text/css">
    .style1
    {
        width: 934px;
    }
    .style5
    {
        width: 70px;
    }
    .style6
    {
        height: 15px;
    }
</style>
<table style="width: 100%; font-family: Times New Roman;" cellspacing="1" cellpadding="1">
    <tr>
        <td align="center" colspan="4">
            <span>
                <asp:Label ID="lblRolePrivilage" runat="server" Text="Role Privilage" SkinID="label"></asp:Label>
            </span>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
        </td>
        <td class="style5" align="left">
            <asp:Label ID="lblRole" runat="server" Text="Role:" Enabled="false" SkinID="label"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlRole" runat="server" OnSelectedIndexChanged="ddlPopulateRole_SelectedIndexChanged"
                AutoPostBack="true" SkinID="ddlCustomer" Enabled="false">
            </asp:DropDownList>
        </td>
    </tr>
</table>
<table cellspacing="1" cellpadding="1">
    <tr align="center">
        <td>
        </td>
        <td class="style1">
            <asp:GridView ID="gvFormsDetails" runat="server" Width="758px" AutoGenerateColumns="false"
                HeaderStyle-BackColor="Chocolate" HeaderStyle-VerticalAlign="Top" Height="100px"
                ToolTip="Form Assignment" BorderColor="#e8c8af" BorderWidth="1px">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                        <HeaderTemplate>
                            Write
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkwrite" runat="server" Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                        <HeaderTemplate>
                            Read
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkRead" runat="server" Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FormName" HeaderText="FormsName" SortExpression="FormName">
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FormDescription" HeaderText="Description" SortExpression="FormDescription">
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField Visible="false">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                        <HeaderTemplate>
                            FormID
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFormId" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "FormID") %> '></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle VerticalAlign="Top" BackColor="Silver"></HeaderStyle>
            </asp:GridView>
        </td>
    </tr>
    </table>
    <table>
    <tr align="center">
        <td>
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="validate()" OnClick="btnSave_Click" />
        </td>
        <td>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
        </td>
    </tr>
</table>
