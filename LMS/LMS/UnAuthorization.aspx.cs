﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UnAuthorization : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblErrorMessage.Text = "You are Un-Authorized to View the Requested Page";
            lblfile.Text = "'" + Request.QueryString["file"].ToString() + "'";
        }
        catch (Exception ex)
        {
            throw ex;
            ///ErrorHandler.RaiseError(ex);
        }
    }
}
