﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using LMSBLL;
using LMSDAL;
using ExceptionalHandling;

using AjaxControlToolkit;



public partial class Query_MyTransaction : System.Web.UI.Page
{

    //public string strErrorMessage;
    //public string strPage;
    //public string[] strResult;
    //public LMSBLL.MyTransactionInfo objInfo;
    //public LMSDAL.MyTransactionDAL objDAL;


    //#region "  Page Events..."

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    //DateDisplay.Text = DateTime.Now.ToString("dddd, MMMM dd");
    //    //txtEndDate.Attributes.Add("onchange", "CheckDate()");

    //    if (!Page.IsPostBack)
    //    {
    //        PopulateMyTransactionDetails();
    //        PopulateItemStatus();
    //        PopulateDate();
    //        //AccessIdentifier();
    //    }
    //}

    //#endregion

    //#region "populate my transaction"

    //private void PopulateMyTransactionDetails()
    //{
    //    try
    //    {
    //        objDAL = new LMSDAL.MyTransactionDAL();
    //        DataView dvwMyTransaction = new DataView();

    //        if ((ViewState["sortExpr"]) != null)
    //        {
    //            dvwMyTransaction.Table = objDAL.GetMyTransactionInfo().Tables[0];
    //            dvwMyTransaction.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
    //        }
    //        else
    //        {
    //            dvwMyTransaction = objDAL.GetMyTransactionInfo().Tables[0].DefaultView;
    //        }
    //        grdMyTransaction.DataSource = dvwMyTransaction;
    //        grdMyTransaction.DataBind();
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler objErrorHandler = new ErrorHandler();
    //        int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
    //        Session["ErrorID"] = intError.ToString();
    //        Response.Redirect("CustomError.aspx");
    //    }
    //    finally
    //    {
    //        objInfo = null;
    //        objDAL = null;
    //    }
    //}
    //#endregion

    //#region "Populate Item Status"
    //public void PopulateItemStatus()
    //{
    //    DataSet dsStatus = new DataSet();
    //    objDAL = new LMSDAL.MyTransactionDAL();
    //    dsStatus = objDAL.MyTransactionItemStatus();

    //    drpitemstatus.DataSource = dsStatus;

    //    drpitemstatus.DataTextField = "Status";
    //    drpitemstatus.DataValueField = "StatusId";

    //    drpitemstatus.DataBind();

    //    drpitemstatus.Items.Insert(0, new ListItem("All", "0"));

       
    //    drpitemstatus.SelectedIndex = drpitemstatus.Items.IndexOf(drpitemstatus.Items.FindByText("Issued"));

    //}
    //#endregion

    //#region "Page Index"

    //protected void grdMyTransaction_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    //{
    //    try
    //    {
    //        grdMyTransaction.PageIndex = e.NewPageIndex;
    //        ViewMyTransaction();
    //        if (objDAL.ViewMyTransactionInfo(objInfo).Tables[0].Rows.Count > 0)
    //        {
    //            grdMyTransaction.DataSource = objDAL.ViewMyTransactionInfo(objInfo).Tables[0];
    //            grdMyTransaction.DataBind();
    //            grdMyTransaction.Visible = true;
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler objErrorHandler = new ErrorHandler();
    //        int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
    //        Session["ErrorID"] = intError.ToString();
    //        Response.Redirect("CustomError.aspx");
    //    }
    //}
    //#endregion

    //protected void grdMyTransaction_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    //{
    //    try
    //    {

    //        objDAL = new LMSDAL.MyTransactionDAL();
    //        ViewState["sortExpr"] = e.SortExpression;
    //        DataView dvItem = new DataView();
    //        if ((String)ViewState["sortDirection"] == "ASC")
    //        {
    //            ViewState["sortDirection"] = "DESC";
    //        }
    //        else
    //        {
    //            ViewState["sortDirection"] = "ASC";
    //        }
    //        ViewMyTransaction();
    //        if (objDAL.ViewMyTransactionInfo(objInfo).Tables[0].Rows.Count > 0)
    //        {
    //            if ((ViewState["sortExpr"]) != null)
    //            {
    //                dvItem.Table = objDAL.ViewMyTransactionInfo(objInfo).Tables[0];
    //                dvItem.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
    //            }
    //            else
    //            {
    //                dvItem = objDAL.ViewMyTransactionInfo(objInfo).Tables[0].DefaultView;
    //            }
    //            grdMyTransaction.DataSource = dvItem;
    //            grdMyTransaction.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler objErrorHandler = new ErrorHandler();
    //        int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
    //        Session["ErrorID"] = intError.ToString();
    //        Response.Redirect("CustomError.aspx");
    //    }
    //}

    //#region " button cancel event"
    //protected void btnCancel_Click(object sender, EventArgs e)
    //{
    //    Cancel();
    //}
    //#endregion

    //#region "cancel function"
    //private void Cancel()
    //{
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    drpitemstatus.SelectedIndex = drpitemstatus.Items.IndexOf(drpitemstatus.Items.FindByText("Issued"));
    //}
    //#endregion

    //#region "button view event"
    //protected void btnView_Click(object sender, EventArgs e)
    //{
    //    txtEndDate.Attributes.Add("OnClick", "CheckDate()");
    //    ViewMyTransaction();
    //}
    //#endregion

    //#region "View My Transaction".
    //private void ViewMyTransaction()
    //{
    ////    var startDate = (Convert.ToDateTime(txtStartDate.Text));
    ////    var endDate = (Convert.ToDateTime(txtEndDate.Text));
       

    ////    if (startDate > endDate)
    ////    {

    ////        string strErrorMsg = "alert('Please Ensure Your End Date is Greater Than Start Date' )";
    ////        ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowInfo", strErrorMsg, true);
    ////    }
    ////    else
    ////    {
    ////    txtEndDate.Attributes.Add("OnClick", "CheckDate()");
    //    try
    //    {
    //        objInfo = new LMSBLL.MyTransactionInfo();
    //        objDAL = new LMSDAL.MyTransactionDAL();
    //        if (txtStartDate.Text == "")
    //        {
    //            objInfo.StartDate = Convert.ToDateTime("01/01/1800");
    //        }
    //        else
    //        {
    //            objInfo.StartDate = Convert.ToDateTime(txtStartDate.Text);
    //        }
    //        if (txtEndDate.Text == "")
    //        {
    //            objInfo.EndDate = Convert.ToDateTime("01/01/1800");
    //        }
    //        else
    //        {
    //            objInfo.EndDate = Convert.ToDateTime(txtEndDate.Text);
    //        }
    //        objInfo.ItemStatus = drpitemstatus.SelectedItem.Text;

    //        if (objDAL.ViewMyTransactionInfo(objInfo).Tables[0].Rows.Count > 0)
    //        {
    //            grdMyTransaction.DataSource = objDAL.ViewMyTransactionInfo(objInfo).Tables[0];
    //            grdMyTransaction.DataBind();
    //            grdMyTransaction.Visible = true;
    //        }
    //        else
    //        {
    //            grdMyTransaction.Visible = false;

    //            string strErrorMsg = "alert('There is no " + drpitemstatus.SelectedItem.Text + " Item on that Particular Date' )";
    //            ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowInfo", strErrorMsg, true);
    //        }
    //    }

    //    catch (Exception ex)
    //    {
    //        ErrorHandler objErrorHandler = new ErrorHandler();
    //        int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
    //         objErrorHandler.RaiseError(ex, strErrorMessage);
    //        Session["ErrorID"] = intError.ToString();
    //        Response.Redirect("CustomError.aspx");
    //    }
    //    finally
    //    {
    //        objInfo = null;
    //        objDAL = null;
    //    }
    //     }
    



    //#endregion

    //#region "Populate Date"
    //private void PopulateDate()
    //{
    //    try
    //    {
    //        DateTime d1 = System.DateTime.Now;
    //        DateTime d2 = System.DateTime.Now.AddDays(-7);
    //        txtStartDate.Text = d2.ToString("MM/dd/yyyy");
    //        txtEndDate.Text = d1.ToString("MM/dd/yyyy");
    //         txtEndDate.Text = (System.DateTime.Now).ToString("MM/dd/yyyy");
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler objErrorHandler = new ErrorHandler();
    //        int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
    //        Session["ErrorID"] = intError.ToString();
    //        Response.Redirect("CustomError.aspx");
    //    }
    //}

    //#endregion

    ////private void AccessIdentifier()
    ////{
    ////    try
    ////    {
    ////        objInfo = new LMSBLL.MyTransactionInfo();
    ////        objDAL = new LMSDAL.MyTransactionDAL();
    ////        //objDAL = new LMSDAL.ItemMaintainDal();
    ////        strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
    ////        WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
    ////        objInfo.LoginId = ExtractUserName(struser.Identity.Name);
    ////        int intLen = 0;
    ////        intLen = strPage.IndexOf(".");
    ////        objInfo.FormName = strPage.Substring(0, intLen);
    ////        strResult = objDAL.Accessverifyer(objInfo);

    ////        if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
    ////        {
    ////            btnView.Enabled = true;
    ////        }
    ////        else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
    ////        {
    ////            btnView.Enabled = false;
    ////        }
    ////        else
    ////        {
    ////            AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane3");
    ////            LinkButton lnkMyTransaction = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkMyTransaction");
    ////            Response.Redirect("~/UnAuthorization.aspx?file=" + lnkMyTransaction.Text.ToString(), false);
    ////            return;
    ////        }
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        ErrorHandler objErrorHandler = new ErrorHandler();
    ////        int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
    ////        Session["ErrorID"] = intError.ToString();
    ////        Response.Redirect("CustomError.aspx");
    ////    }
    ////    finally
    ////    {
    ////        objInfo = null;
    ////        objDAL = null;
    ////    }
    ////}

    //public string ExtractUserName(string path)
    //{
    //    // try
    //    // {
    //    string[] userPath = path.Split(new char[] { '\\' });

    //    return userPath[userPath.Length - 1];
    //  }
    // //catch (Exception ex)

    // //{
    // //ErrorHandler.RaiseError(ex, strLogError);
    // //} 
    //}

}



        


