﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyTransaction.ascx.cs" Inherits="Query_MyTransaction" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
   
    <script language="javascript" type="text/javascript">


       function CheckDate() {
           var date1 = document.getElementById('<%= txtStartDate.ClientID %>').value;
           var date2 = document.getElementById('<%= txtEndDate.ClientID %>').value;
           if (date1 > date2) {
                alert('Please Ensure Your End Date is Greater Than Start Date ');
                document.getElementById('<%= txtEndDate.ClientID %>').value = date2;
            }



       }

    </script>

    <asp:UpdatePanel runat="server" ID="updcontract">
        <ContentTemplate>
            <table style="width: 100%; font-family: Verdana; font-size: 11px; height: 100%">
                <%--id="tblPageLevel">--%>
                <tr class="TRHeader">
                    <td align="Center" colspan="4">
                        <span>MyTransaction List</span>
                    </td>
                </tr>
                <tr><td /></tr>
                <tr>
                    <td>
                        <asp:Label ID="lblstartdate" CssClass="label" runat="server" Text="Start Date"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtStartDate" runat="server" SkinID="TxtCalender" TabIndex="1" ToolTip="Enter the Start Date.">
                        </asp:TextBox>
                        <asp:ImageButton ID="btnstartdata" runat="server" ImageUrl="~/images/Calendar.png" />
                        <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtStartDate" PopupButtonID="btnstartdata"
                            Format="MM/dd/yyyy" runat="server">
                        </cc1:CalendarExtender>
                    </td>
                    <td>
                        <asp:Label ID="lblEndDate" CssClass="label" runat="server" Text="End Date"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEndDate" runat="server" TabIndex="2" ToolTip="Enter the End Date."
                            SkinID="TxtCalender">
                        </asp:TextBox>
                        <asp:ImageButton ID="btnenddate" runat="server" ImageUrl="~/images/Calendar.png" />
                        <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="txtEndDate" PopupButtonID="btnenddate"
                            Format="MM/dd/yyyy" runat="server">
                        </cc1:CalendarExtender>
                        <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End Date should be Greater than Start Date"
                            ControlToCompare="txtEndDate" ControlToValidate="txtStartDate" Display="Dynamic"
                            Operator="LessThanEqual" Type="Date">
                        </asp:CompareValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td style="width: 77px" />
                </tr>
                <tr>
                    <td style="width: 77px">
                        <asp:Label ID="lblitemstatus" CssClass="label" runat="server" Text="Item Status"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="drpitemstatus" runat="server" TabIndex="3" ToolTip="select item status">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 77px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 77px">
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td align="left" style="width: 832px">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnView" runat="server" Text="View" TabIndex="4" ToolTip="Click here to View Your Transaction"
                            OnClick="btnView_Click"></asp:Button>
                        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" TabIndex="5" ToolTip="Click here to cancel"
                            OnClick="btnCancel_Click" OnClientClick="confirmationCancel()">
                            </asp:Button>
                    </td>
                </tr>
            </table>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updcontract" ID="PageProgress"
                            DisplayAfter="1">
                            <ProgressTemplate>
                                <table align="center">
                                    <tr>
                                        <td>
                                            <asp:Image runat="server" ImageUrl="~/images/Progress.gif" Height="20px" Width="20px"
                                                ID="imgWait"></asp:Image>
                                        </td>
                                        <td>
                                            <span>
                                                <asp:Label runat="server" Text="Processing...Please Wait!" ID="lblProcessingAnim"></asp:Label></span>
                                        </td>
                                    </tr>
                                </table>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <%--  <asp:GridView ID="grdSearch" runat="server" Width="100%" AutoGenerateColumns="false"
                    AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="#e8c8af" HeaderStyle-VerticalAlign="Top"
                    OnPageIndexChanging="grdSearch_PageIndexChanging">
                  --%>
                    <td style="width: 832px">
                        <asp:GridView ID="grdMyTransaction" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowPaging="true" AllowSorting="true" OnPageIndexChanging="grdMyTransaction_PageIndexChanging"
                            HeaderStyle-BackColor="#e8c8af" HeaderStyle-VerticalAlign="top" BorderColor="#e8c8af"
                            BorderWidth="1px" OnSorting="grdMyTransaction_Sorting" 
                            HorizontalAlign="Center">
                            <%--BorderColor="#DEBA84" >--%>
                            <%--  <AlternatingRowStyle CssClass="gvHeaderStyle" />--%>
                            <Columns>
                                <asp:BoundField DataField="ItemType" HeaderText="Item Type" SortExpression="ItemType">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" Width="50" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="180" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CATEGORY" HeaderText="Category" SortExpression="CATEGORY">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IssueDate" HeaderText="Issue Date" SortExpression="IssueDate">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" Width="50" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DueDate" HeaderText="Due Date" SortExpression="DueDate">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" Width="50" />
                                </asp:BoundField>
                                <%-- <asp:BoundField DataField="NumberofRenewal" HeaderText="Number of Renewal" SortExpression="NumberofRenewal">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="70" />
                                </asp:BoundField>--%>
                                <asp:BoundField DataField="ReturnDate" HeaderText="Return Date" SortExpression="ReturnDate">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" Width="50" />
                                </asp:BoundField>
                               <%-- <asp:BoundField DataField="Status" HeaderText="Item Status" SortExpression="Status">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" Width="60" />
                                </asp:BoundField>--%>
                                <%--<asp:TemplateField Visible="False" HeaderText="CATEGORY_ID">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategoryId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CATEGORY_ID") %>'
                                            Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                            <HeaderStyle BackColor="#E8C8AF" VerticalAlign="Top" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
             <script language="javascript" type="text/javascript">



                 function confirmationCancel() {

                     if (confirm("Are you sure Want to Cancel?") == true)
                         return true;
                     else
                         return false;
                 }
 
    
    </script>
        </ContentTemplate>
    </asp:UpdatePanel>
 