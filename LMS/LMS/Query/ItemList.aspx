<%@ Page Title="Item List" Theme="LMS" Language="C#" MasterPageFile="~/LMSMaster.master"
    AutoEventWireup="true" CodeFile="ItemList.aspx.cs" Inherits="Master_SearchPage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="Server">
    <asp:UpdatePanel runat="server" ID="upSearch">
        <ContentTemplate>
            <table style="width: 100%; font-family: Calibri; font-size: 11px; height: 100%">
                <tr class="TRHeader">
                    <td align="Center" colspan="4">
                        <span>Item List</span>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblItem_Type" CssClass="label" runat="server" Text="Item Type"></asp:Label>
                    </td>
                    <td style="width: 302px">
                      <asp:DropDownList ID="ddItemType" runat="server" AutoPostBack="true" TabIndex="1"
                            CssClass="" ToolTip="Select the Item Type" SkinID="ddlCustomer" OnSelectedIndexChanged="ddItemType_SelectedIndexChanged">
                        </asp:DropDownList>--%>
                       <%-- <telerik:RadComboBox ID="ddItemType" runat="server" 
                            onselectedindexchanged="ddItemType_SelectedIndexChanged1">
                        </telerik:RadComboBox>--%>
                    </td>
                    <td>
                        <asp:Label ID="lblCategory" CssClass="label" runat="server" Text="Category"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddCategory" runat="server" TabIndex="2" SkinID="ddlCustomer"
                            OnSelectedIndexChanged="ddCategory_SelectedIndexChanged" Style="height: 22px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblAuthor" CssClass="label" runat="server" Text="Author"></asp:Label>
                    </td>
                    <td style="width: 302px">
                        <asp:DropDownList ID="ddAuthor" runat="server" AutoPostBack="true" TabIndex="3" ToolTip="Select The Author"
                            SkinID="ddlCustomer" OnSelectedIndexChanged="ddAuthor_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="lblTitle" CssClass="label" runat="server" Text="Item Title"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddTitle" runat="server" AutoPostBack="true" TabIndex="4" ToolTip="Select the Title"
                            SkinID="ddlCustomer" OnSelectedIndexChanged="ddTitle_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="left">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="butSearch" runat="server" Text="Search" TabIndex="5" ToolTip="Click here to Search Keyword"
                            OnClick="butSearch_Click" />
                        &nbsp;
                        <asp:Button ID="butCancel" runat="server" Text="Cancel" TabIndex="6" ToolTip="Click here to cancel"
                            OnClick="butCancel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="upSearch" ID="PageProgress"
                            DisplayAfter="1">
                            <ProgressTemplate>
                                <table align="center">
                                    <%--<tr>
                                        <td align="center">
                                            <asp:Image runat="server" ImageUrl="~/images/Progress.gif" Height="20px" Width="20px"
                                                ID="imgWait"></asp:Image>
                                        </td>
                                        <td>
                                            <span>
                                                <asp:Label runat="server" Text="Processing...Please Wait!" ID="lblProcessingAnim"></asp:Label></span>
                                        </td>
                                    </tr>--%>
                                </table>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:GridView ID="grdSearch" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="#e8c8af" HeaderStyle-VerticalAlign="Top"
                            OnPageIndexChanging="grdSearch_PageIndexChanging" OnSorting="grdSearch_Sorting"
                            BorderColor="#e8c8af" BorderWidth="1px">
                            <%-- OnSorting="grdSearch_Sorting"--%>
                            <Columns>
                                <%--<asp:CommandField ShowSelectButton="True">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:CommandField>--%>
                                <asp:BoundField DataField="ItemType" HeaderText="Item Type" SortExpression="ItemType">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="30" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TITLE" HeaderText="Title" SortExpression="TITLE">
                                    <%--SortExpression="TITLE"--%>
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="120" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CATEGORY" HeaderText="Category" SortExpression="CATEGORY">
                                    <%--SortExpression="CATEGORY"--%>
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="100" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AUTHOR" HeaderText="Author" SortExpression="AUTHOR">
                                    <%-- SortExpression="AUTHOR"--%>
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="80" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                                    <%--SortExpression="Status"--%>
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Center" Width="50" />
                                </asp:BoundField>
                                <%-- <asp:TemplateField Visible="False" HeaderText="BOOK_ID">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblItem" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ITEM_ID") %>'
                                    Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                            </Columns>
                            <EmptyDataTemplate>
                                &nbsp;
                            </EmptyDataTemplate>
                            <HeaderStyle VerticalAlign="Top" BackColor="Silver"></HeaderStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; font-family: Calibri; font-size: 11px; height: 100%"> 
                <tr>
                    <td>
                        <telerik:RadGrid ID="gvRadSearch" runat="server" PageSize="10" AllowPaging="true"
                            AllowSorting="true" OnNeedDataSource="gvRadSearch_NeedDataSource">
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>