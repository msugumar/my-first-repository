﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using LMSBLL;
using LMSDAL;
using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using AjaxControlToolkit;
using ExceptionalHandling;

public partial class Master_SearchPage : System.Web.UI.Page
{
    public string strPage;
    public string[] strResult;
    public string strLogError = string.Empty;
    public LMSBLL.SearchInfo objInfo;
    public LMSDAL.SearchDAL objDAL;
    public string strErrorMessage;

    #region "Page Load"
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //PopulateSearchDetails();
                AccessIdentifier();
                PopulateTitleDropDown();
                PopulateCategoryDropDown();
                PopulateItemTypeDropDown();
                PopulateAuthorDropDown();
                gvRadSearch.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }
    #endregion

    #region "Table Sorting & Paging"
    protected void grdSearch_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            grdSearch.PageIndex = e.NewPageIndex;
            ParameterFunction();
            if (objDAL.PutSearchDetail(objInfo).Tables[0].Rows.Count > 0)
            {
                grdSearch.DataSource = objDAL.PutSearchDetail(objInfo).Tables[0];
                grdSearch.DataBind();
                grdSearch.Visible = true;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    } //gridview page index changing ends

    protected void grdSearch_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            objDAL = new LMSDAL.SearchDAL();
            ViewState["sortExpr"] = e.SortExpression;
            DataView dvItem = new DataView();
            if ((String)ViewState["sortDirection"] == "ASC")
            {
                ViewState["sortDirection"] = "DESC";
            }
            else
            {
                ViewState["sortDirection"] = "ASC";
            }
            ParameterFunction();
            if (objDAL.PutSearchDetail(objInfo).Tables[0].Rows.Count > 0)
            {
                if ((ViewState["sortExpr"]) != null)
                {
                    dvItem.Table = objDAL.PutSearchDetail(objInfo).Tables[0];
                    dvItem.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
                }
                else
                {
                    dvItem = objDAL.PutSearchDetail(objInfo).Tables[0].DefaultView;
                }
                grdSearch.DataSource = dvItem;
                grdSearch.DataBind();
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }
    #endregion

    #region " Search for the Particular Items from the DB - (Search )"
    protected void butSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if ((ddAuthor.SelectedItem.Text != "Select Here") || (ddCategory.SelectedItem.Text != "Select Here") || (ddItemType.SelectedItem.Text != "Select Here") || (ddTitle.SelectedItem.Text != "Select Here"))
            {
                SearchDetailInfo();
            }
            else
            {
                gvRadSearch.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Select Reqired Fields !')</script>", false);
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            objErrorHandler.RaiseError(ex, strErrorMessage);
        }
    }

    protected void ParameterFunction()
    {
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            objInfo.SearchTitle = ddTitle.SelectedItem.Text;
            objInfo.SearchCategory = ddCategory.SelectedItem.Value;
            objInfo.SearchItemType = ddItemType.SelectedItem.Value;
            objInfo.SearchAuthor = ddAuthor.SelectedItem.Text;
            objInfo.SearchItemID = Convert.ToInt32(ddItemType.SelectedValue);
            objInfo.SearchCategoryID = Convert.ToInt32(ddCategory.SelectedValue);
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }

    protected void SearchDetailInfo()
    {
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            objInfo.SearchTitle = ddTitle.SelectedItem.Text;
            objInfo.SearchCategory = ddCategory.SelectedItem.Value;
            objInfo.SearchItemType = ddItemType.SelectedItem.Value;
            objInfo.SearchAuthor = ddAuthor.SelectedItem.Text;
            objInfo.SearchItemID = Convert.ToInt32(ddItemType.SelectedValue);
            objInfo.SearchCategoryID = Convert.ToInt32(ddCategory.SelectedValue);


            if (objDAL.PutSearchDetail(objInfo).Tables[0].Rows.Count > 0)
            {
                gvRadSearch.DataSource = objDAL.PutSearchDetail(objInfo).Tables[0];
                gvRadSearch.DataBind();
                gvRadSearch.Visible = true;
            }
            else
            {
                gvRadSearch.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Item Not Found!')</script>", false);
            }
            gvRadSearch_NeedDataSource(null, null);
            gvRadSearch.DataBind();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    #endregion

    #region "Populate the Items in the DropDown Box"

    public void PopulateTitleDropDown()
    {
        try
        {
            objDAL = new LMSDAL.SearchDAL();
            ddTitle.DataSource = objDAL.GetTitleInfo().Tables[0];
            ddTitle.DataTextField = "Title";
            ddTitle.DataValueField = "ItemId";
            ddTitle.DataBind();
            //   ddTitle.Items.Insert(0, "Select Here");

            //ddTitle.Items.Insert(0, new ListItem("Select Here", "-1"));
            ddTitle.Items.Insert(0, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }

    public void PopulateCategoryDropDown()
    {
        try
        {
            objDAL = new LMSDAL.SearchDAL();
            ddCategory.DataSource = objDAL.GetCategoryInfo().Tables[0];
            ddCategory.DataTextField = "Category";
            ddCategory.DataValueField = "CategoryId";
            ddCategory.DataBind();
            //  ddCategory.Items.Insert(0, "Select Here");

            // ddCategory.Items.Insert(0, new ListItem("Select Here", "-1"));
            ddCategory.Items.Insert(0, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }

    public void PopulateAuthorDropDown()
    {
        try
        {
            objDAL = new LMSDAL.SearchDAL();
            ddAuthor.DataSource = objDAL.GetAuthorInfo().Tables[0];
            ddAuthor.DataTextField = "Author";
            ddAuthor.DataValueField = "ItemId";
            ddAuthor.DataBind();
            // ddAuthor.Items.Insert(0, "Select Here");
            // ddAuthor.Items.Insert(0, new ListItem("Select Here", "-1"));
            ddAuthor.Items.Insert(0, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }

    public void PopulateItemTypeDropDown()
    {
        try
        {
            objDAL = new LMSDAL.SearchDAL();
            ddItemType.DataSource = objDAL.GetItemTypeInfo().Tables[0];
            ddItemType.DataTextField = "ItemType";
            ddItemType.DataValueField = "ItemTypeId";
            ddItemType.DataBind();
            // ddItemType.Items.Insert(0, new ListItem("Select Here", "-1"));
           // ddItemType.Text = "All";

           ddItemType.Items.Insert(0, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }

    protected void butCancel_Click(object sender, EventArgs e)
    {
        Cancel();
    }

    private void Cancel()
    {
        try
        {
            PopulateTitleDropDown();
            PopulateCategoryDropDown();
            PopulateItemTypeDropDown();
            PopulateAuthorDropDown();
            grdSearch.Visible = false;
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }
    #endregion

    #region "Selected Index for Dropdown Item Type"

    protected void ddItemType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //FiterByItemTypeInfo();
    }

    private void FiterByItemTypeInfo()
    {
        DataSet dsFilterItemTypeInfo = new DataSet();
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            objInfo.SearchItemID = Convert.ToInt32(ddItemType.SelectedValue);
            if (ddItemType.SelectedIndex == 0)
            {
                PopulateCategoryDropDown();
                PopulateItemTypeDropDown();
                PopulateTitleDropDown();
                PopulateAuthorDropDown();
                grdSearch.Visible = false;
            }
            else
            {
                if (ddCategory.SelectedIndex == 0 || ddTitle.SelectedIndex == 0 || ddAuthor.SelectedIndex == 0)
                {

                    dsFilterItemTypeInfo = objDAL.GetFiterByItemTypeInfo(objInfo);
                    if (dsFilterItemTypeInfo.Tables.Count > 0)
                    {
                        if (dsFilterItemTypeInfo.Tables[0].Rows.Count > 0)
                        {
                            ddCategory.Items.Clear();
                            ddCategory.DataSource = dsFilterItemTypeInfo.Tables[0];
                            ddCategory.DataBind();
                            ddCategory.DataTextField = "Category";
                            ddCategory.DataValueField = "CategoryId";
                            ddCategory.Items.Insert(0, new ListItem("All", "0"));
                        }
                        if (dsFilterItemTypeInfo.Tables[1].Rows.Count > 0)
                        {
                            ddTitle.Items.Clear();
                            ddTitle.DataSource = dsFilterItemTypeInfo.Tables[1];
                            ddTitle.DataBind();
                            ddTitle.DataTextField = "Title";
                            ddTitle.DataValueField = "ItemId";
                            ddTitle.Items.Insert(0, new ListItem("All", "0"));
                        }
                        if (dsFilterItemTypeInfo.Tables[2].Rows.Count > 0)
                        {
                            ddAuthor.Items.Clear();
                            ddAuthor.DataSource = dsFilterItemTypeInfo.Tables[2];
                            ddAuthor.DataBind();
                            ddAuthor.DataTextField = "Author";
                            ddAuthor.DataValueField = "ItemId";
                            ddAuthor.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Item Not Found!')</script>", false);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    #endregion

    #region "Selected Index for Dropdown Category"

    protected void ddCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
      // FiterByCategoryInfo();
    }

    private void FiterByCategoryInfo()
    {
        DataSet dsFilterCategoryInfo = new DataSet();
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            objInfo.SearchCategoryID = Convert.ToInt32(ddCategory.SelectedValue);
            dsFilterCategoryInfo = objDAL.GetFiterByCategoryInfo(objInfo);
            if (dsFilterCategoryInfo.Tables.Count > 0)
            {
                //if (dsFilterCategoryInfo.Tables[0].Rows.Count > 0)
                //{
                //    ddItemType.Items.Clear();
                //    ddItemType.DataSource = dsFilterCategoryInfo.Tables[0];
                //    ddItemType.DataBind();
                //    ddItemType.DataTextField = "ItemType";
                //    ddItemType.DataValueField = "ItemTypeId";
                //    ddItemType.Items.Insert(0, new ListItem("All", "0"));
                //}
                if (dsFilterCategoryInfo.Tables[1].Rows.Count > 0)
                {
                    ddTitle.Items.Clear();
                    ddTitle.DataSource = dsFilterCategoryInfo.Tables[1];
                    ddTitle.DataBind();
                    ddTitle.DataTextField = "Title";
                    ddTitle.DataValueField = "ItemId";
                    ddTitle.Items.Insert(0, new ListItem("All", "0"));
                }
                if (dsFilterCategoryInfo.Tables[2].Rows.Count > 0)
                {
                    ddAuthor.Items.Clear();
                    ddAuthor.DataSource = dsFilterCategoryInfo.Tables[2];
                    ddAuthor.DataBind();
                    ddAuthor.DataTextField = "Author";
                    ddAuthor.DataValueField = "ItemId";
                    ddAuthor.Items.Insert(0, new ListItem("All", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Item Not Found!')</script>", false);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    #endregion

    #region "Selected Index for Dropdown Author"
    protected void ddAuthor_SelectedIndexChanged(object sender, EventArgs e)
    {
        // FiterByAuthorInfo();
    }
    private void FiterByAuthorInfo()
    {
        DataSet dsFilterAuthorInfo = new DataSet();
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            objInfo.SearchAuthor = ddAuthor.SelectedValue;
            dsFilterAuthorInfo = objDAL.GetFiterByAuthorInfo(objInfo);
            if (dsFilterAuthorInfo.Tables.Count > 0)
            {
                if (dsFilterAuthorInfo.Tables[0].Rows.Count > 0)
                {
                    ddItemType.Items.Clear();
                    ddItemType.DataSource = dsFilterAuthorInfo.Tables[0];
                    ddItemType.DataBind();
                    ddItemType.DataTextField = "ItemType";
                    ddItemType.DataValueField = "ItemTypeId";
                    ddItemType.Text = "All";

                    //ddItemType.Items.Insert(0, new ListItem("All", "0"));
                }
                if (dsFilterAuthorInfo.Tables[1].Rows.Count > 0)
                {
                    ddCategory.Items.Clear();
                    ddCategory.DataSource = dsFilterAuthorInfo.Tables[1];
                    ddCategory.DataBind();
                    ddCategory.DataTextField = "Category";
                    ddCategory.DataValueField = "CategoryId";
                    ddCategory.Items.Insert(0, new ListItem("All", "0"));
                }
                if (dsFilterAuthorInfo.Tables[2].Rows.Count > 0)
                {
                    ddTitle.Items.Clear();
                    ddTitle.DataSource = dsFilterAuthorInfo.Tables[2];
                    ddTitle.DataBind();
                    ddTitle.DataTextField = "Title";
                    ddTitle.DataValueField = "ItemId";
                    ddTitle.Items.Insert(0, new ListItem("All", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Item Not Found!')</script>", false);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    #endregion

    #region "Selected Index for Dropdown Title"
    protected void ddTitle_SelectedIndexChanged(object sender, EventArgs e)
    {
        // FiterByTitleInfo();
    }
    private void FiterByTitleInfo()
    {
        DataSet dsFilterTitleInfo = new DataSet();
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            objInfo.SearchTitle = ddTitle.SelectedValue;
            dsFilterTitleInfo = objDAL.GetFiterByTitleInfo(objInfo);
            if (dsFilterTitleInfo.Tables.Count > 0)
            {
                if (dsFilterTitleInfo.Tables[0].Rows.Count > 0)
                {
                    ddItemType.Items.Clear();
                    ddItemType.DataSource = dsFilterTitleInfo.Tables[0];
                    ddItemType.DataBind();
                    ddItemType.DataTextField = "ItemType";
                    ddItemType.DataValueField = "ItemTypeId";
                    //ddItemType.Text = "All";
                   // ddItemType.Items.Insert(0, new ListItem("All", "0"));
                }
                if (dsFilterTitleInfo.Tables[1].Rows.Count > 0)
                {
                    ddCategory.Items.Clear();
                    ddCategory.DataSource = dsFilterTitleInfo.Tables[1];
                    ddCategory.DataBind();
                    ddCategory.DataTextField = "Category";
                    ddCategory.DataValueField = "CategoryId";
                    ddCategory.Items.Insert(0, new ListItem("All", "0"));
                }
                if (dsFilterTitleInfo.Tables[2].Rows.Count > 0)
                {
                    ddAuthor.Items.Clear();
                    ddAuthor.DataSource = dsFilterTitleInfo.Tables[2];
                    ddAuthor.DataBind();
                    ddAuthor.DataTextField = "Author";
                    ddAuthor.DataValueField = "ItemId";
                    ddAuthor.Items.Insert(0, new ListItem("All", "0"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Item Not Found!')</script>", false);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    #endregion

    #region "Access Identification "
    private void AccessIdentifier()
    {
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            //objDAL = new LMSDAL.ItemMaintainDal();
            strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
            WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            objInfo.LoginId = ExtractUserName(struser.Identity.Name);
            int intLen = 0;
            intLen = strPage.IndexOf(".");
            objInfo.FormName = strPage.Substring(0, intLen);
            strResult = objDAL.Accessverifyer(objInfo);

            if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
            {
                butSearch.Enabled = true;
            }
            else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
            {
                butSearch.Enabled = false;
            }
            else
            {
                AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane3");
                LinkButton lnkItemMaintain = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkItemList");
                Response.Redirect("~/UnAuthorization.aspx?file=" + lnkItemMaintain.Text.ToString(), false);
                //return;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    public string ExtractUserName(string path)
    {
        /*  try
          {*/
        string[] userPath = path.Split(new char[] { '\\' });

        return userPath[userPath.Length - 1];
        /*      }
              catch (Exception ex)
         * 
              {
                  ErrorHandler.RaiseError(ex, strLogError);
              } */
    }
    #endregion
    protected void gvRadSearch_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            objInfo = new LMSBLL.SearchInfo();
            objDAL = new LMSDAL.SearchDAL();
            objInfo.SearchTitle = ddTitle.SelectedItem.Text;
            objInfo.SearchCategory = ddCategory.SelectedItem.Value;
            objInfo.SearchItemType = ddItemType.SelectedItem.Value;
            objInfo.SearchAuthor = ddAuthor.SelectedItem.Text;
            objInfo.SearchItemID = Convert.ToInt32(ddItemType.SelectedValue);
            objInfo.SearchCategoryID = Convert.ToInt32(ddCategory.SelectedValue);


            if (objDAL.PutSearchDetail(objInfo).Tables[0].Rows.Count > 0)
            {
                grdSearch.DataSource = objDAL.PutSearchDetail(objInfo).Tables[0];
                grdSearch.Visible = true;
            }
            else
            {
                grdSearch.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Item Not Found!')</script>", false);
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    protected void ddItemType_SelectedIndexChanged1(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
      //  FiterByItemTypeInfo();
    }
}