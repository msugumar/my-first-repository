<%@ Page Language="C#" Title="Transaction" Theme="LMS" MasterPageFile="~/LMSMaster.master"
    AutoEventWireup="true" CodeFile="Transation.aspx.cs" Inherits="Master_issue" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LMSCPH" runat="Server">
    <asp:UpdatePanel ID="updtans" runat="server" UpdateMode="Conditional">
<ContentTemplate>
    <table width="100%" cellpadding="1" cellspacing="1">
        <tr class="TRHeader">
            <td align="center" colspan="4">
                <span>Transaction</span>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr> 
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblItemType" runat="server" CssClass="label" Text="ItemType"></asp:Label>
                <span style="font-weight: bold; color: Red;">*</span>
            </td>
            <td>
                <asp:DropDownList ID="ddlItemType" runat="server" ToolTip="Select the ItemType" OnSelectedIndexChanged="ddlItemType_SelectedIndexChanged"
                    AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblItem" runat="server" CssClass="label" Text="Item"></asp:Label>
                <span style="font-weight: bold; color: Red;">*</span>
            </td>
            <td>
                <asp:DropDownList ID="ddlItem" ToolTip="Select the Item" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
         <tr> 
            <td>
            </td>
        </tr>
        <tr>
            <td> 
                <asp:Label ID="lblIssuedTo" runat="server" CssClass="label" Text="IssuedTo"></asp:Label>
                <span style="font-weight: bold; color: Red;">*</span>
            </td>
            <td>
                <asp:DropDownList ID="ddlIssuedTo" ToolTip="Select the User" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblIssueDate" CssClass="label" runat="server" Text="IssueDate"></asp:Label>
                <span style="font-weight: bold; color: Red;">*</span>
            </td>
            <td>
                <asp:TextBox ID="txtIssueDate" runat="server" 
                    SkinID="TxtCalender" ToolTip="Select the IssueDate"></asp:TextBox>
                <asp:ImageButton ID="btnIssueDate" ToolTip="Select the IssueDate" runat="server"
                    ImageUrl="~/images/Calendar.png" />
                <cc1:CalendarExtender ID="Calendarextender2" runat="server" TargetControlID="txtIssueDate"
                    Enabled="true" Format="MM/dd/yyyy" PopupButtonID="btnIssueDate">
                </cc1:CalendarExtender>
            </td> 
        <tr>   
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblDueDate" CssClass="label" runat="server" Text="DueDate"></asp:Label>
                <span style="font-weight: bold; color: Red;">*</span>
            </td>
            <td>
                <asp:TextBox ID="txtDueDate" runat="server"  
                    SkinID="TxtCalender" ToolTip="Select the DueDate"></asp:TextBox>
                <asp:ImageButton ID="btnDueDate" ToolTip="Select the DueDate" runat="server" ImageUrl="~/images/Calendar.png"
                    Width="16px" />
                <cc1:CalendarExtender ID="Calendarextender1" runat="server" TargetControlID="txtDueDate"
                    Enabled="true" Format="MM/dd/yyyy" PopupButtonID="btnDueDate">
                </cc1:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblReturned" CssClass="label" runat="server" Text="Returned"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="chkReturned" Enabled="false" runat="server" />
            </td>
            <td style="height: 13px">
                <asp:Label ID="lblReturnDate" CssClass="label" runat="server" Text="ReturnDate"></asp:Label>
            </td>
            <td style="height: 13px">
                <asp:TextBox ID="txtReturnDate" runat="server" 
                    Enabled="false" SkinID="TxtCalender" ToolTip="Select the ReturnDate"></asp:TextBox>
                <asp:ImageButton ID="btnReturnDate" runat="server" Enabled="false" ToolTip="Select the ReturnDate" ImageUrl="~/images/Calendar.png" />
                <cc1:CalendarExtender ID="Calendarextender3" runat="server" TargetControlID="txtReturnDate"
                    Format="MM/dd/yyyy" PopupButtonID="btnReturnDate">
                </cc1:CalendarExtender>
            </td>
        </tr>
            <tr> 
            <td>
                <div style="display:none">
                <asp:LinkButton ID="lbtnrenew" runat="server" Visible="false">Renewal</asp:LinkButton></div>
                <asp:LinkButton ID="lbtnRenewal" runat="server" ForeColor="Blue" CssClass="label" OnClick="lbtnRenewal_Click" Enabled="false">Renewal</asp:LinkButton><%--skinid="lnkBrown"--%>
            </td>
        </tr>
        </table>
        <table>
        <tr>
            <td align="center">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" ToolTip="Click To Save" OnClick="btnSave_Click" OnClientClick="return validate()" />
                &nbsp;&nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" ToolTip="Click To Cancel" OnClick="btnCancel_Click" OnClientClick="return cancel()" />
            </td>
        </tr>
        </table>
        <table width="100%">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
              <td colspan="4">
                <asp:GridView ID="gvIssue" runat="server" Width="100%" AutoGenerateColumns="false"
                    AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="silver" HeaderStyle-VerticalAlign="top"
                    OnPageIndexChanging="gvIssue_PageIndexChanging" OnSorting="gvIssue_Sorting" OnRowCommand="gvIssue_RowCommand">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="30" />
                        </asp:CommandField>
                        <asp:BoundField DataField="Title" HeaderText="Item" SortExpression="Title">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" Width="90"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="UserName" HeaderText="IssuedTo" SortExpression="UserName">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" Width="70" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IssueDate" HeaderText="IssueDate" SortExpression="IssueDate">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="40" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="Status" Visible="false" SortExpression="Status">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="60" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ReturnDate" HeaderText="ReturnDate" SortExpression="ReturnDate">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="40" />
                        </asp:BoundField>
                        <asp:TemplateField Visible="false" HeaderText="DueDate" SortExpression="DueDate">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDueDate" runat="server" Text='<%#Bind("DueDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false" HeaderText="ItemType" SortExpression="ItemType">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblItemType" runat="server" Text='<%#Bind("ItemType")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="False" HeaderText="TransactionId">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTransactionId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TransactionId")%>'
                                    Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
      <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById("<%=ddlItemType.ClientID%>").selectedIndex == "") {
                alert("Please Select The Item Type");
                document.getElementById("<%=ddlItemType.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=ddlItem.ClientID%>").selectedIndex == "") {
                alert("Please Select The Item");
                document.getElementById("<%=ddlItem.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=ddlIssuedTo.ClientID%>").selectedIndex == "") {
                alert("Please Select User Its Empty Now");
                document.getElementById("<%=ddlIssuedTo.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txtIssueDate.ClientID%>").value == "") {
                alert("Please Select The Issuedate");
                document.getElementById("<%=txtIssueDate.ClientID%>").focus();
                return false;
            }

            if (document.getElementById("<%=txtDueDate.ClientID%>").value == "") {
                alert("Please Selects The Duedate");
                document.getElementById("<%=txtDueDate.ClientID%>").focus();
                return false;
            }
            if (document.getElementById('<%=txtReturnDate.ClientID%>').value == "") {
                var IssueDate = document.getElementById('<%=txtIssueDate.ClientID%>').value;
                var DueDate = document.getElementById('<%=txtDueDate.ClientID%>').value;
                if (IssueDate > DueDate) {
                    alert("Please Ensure That Due Date is Greater Than Issue Date ");
                    document.getElementById("<%=txtDueDate.ClientID%>").value = DueDate;
                    return false;
                }
            }
            if (document.getElementById('<%=txtReturnDate.ClientID%>').value != "") {
                var Issue = document.getElementById('<%=txtIssueDate.ClientID%>').value;
                var ReturnDate = document.getElementById('<%=txtReturnDate.ClientID%>').value
                if (Issue > ReturnDate) {
                    alert("Please Ensure That Return Date is Greater Than Issue Date ");
                    document.getElementById("<%=txtReturnDate.ClientID%>").value = ReturnDate;
                    return false;
                }
            }
            var a;
            a = confirm("Do you really want to Save ?");
            if (!a) {
                return false;
            }
        }
        function cancel() {
            var a;
            a = confirm("Do you really want to Cancel ?");
            if (!a) {
                return false;
            }
        }   
        function check() {
            document.getElementById('<%=txtReturnDate.ClientID%>').disabled = false;
            document.getElementById('<%=btnReturnDate.ClientID%>').disabled = false;
            if (document.getElementById("<%=chkReturned.ClientID%>").checked == true) {
                var dt = new Date();
                var thedate = dt.getDate();                                             
                var themonth = (dt.getMonth()) + 1;
                var theyear = dt.getFullYear();
                if (thedate < 10) {
                    thedate = ("0" + thedate);
                }
                if (themonth < 10) {
                    themonth = ("0" + themonth);
                }
                var currentdate = (themonth + "/" + thedate + "/" + theyear)
                document.getElementById('<%=txtReturnDate.ClientID%>').value = currentdate;
            }
            else {
                document.getElementById('<%=txtReturnDate.ClientID%>').value = "";
                document.getElementById('<%=txtReturnDate.ClientID%>').disabled = true;
                document.getElementById('<%=btnReturnDate.ClientID%>').disabled = true;
            }
        }
        function duedatevalidate() {
            var IssueDate = document.getElementById('<%=txtIssueDate.ClientID%>').value;
            var DueDate = document.getElementById('<%=txtDueDate.ClientID%>').value;
            if (IssueDate > DueDate) {
                alert("Please Ensure That Due Date is Greater Than Issue Date ");
                document.getElementById("<%=txtDueDate.ClientID%>").value = DueDate;
                return false;
            }
        }
        function returndatevalidate() {
            var IssueDate = document.getElementById('<%=txtIssueDate.ClientID%>').value;
            var ReturnDate = document.getElementById('<%=txtReturnDate.ClientID%>').value;
            if (IssueDate > ReturnDate) {
                alert("Please Ensure That Return Date is Greater Than Issue Date ");
                document.getElementById("<%=txtReturnDate.ClientID%>").value = ReturnDate;
                return false;
            }
        }
        function IssueDatevalidate() {
            var IssueDate = document.getElementById('<%=txtIssueDate.ClientID%>').value;
            var dt = new Date(IssueDate);
            dt.setDate(dt.getDate() + 7);
            var thedate = (dt.getDate());
            var themonth = (dt.getMonth() + 1);
            var theyear = dt.getFullYear();
            if (thedate < 10) {
            thedate=("0" + thedate);
            }
            if (themonth < 10) {
                themonth = ("0" + themonth);
            }
            var currentdate = (themonth + "/" + thedate + "/" + theyear)
            document.getElementById('<%=txtDueDate.ClientID%>').value = currentdate;
        }
        function pass() {
            var strDueDate = document.getElementById('<%=txtPass.ClientID%>').value;
            document.getElementById('<%=txtDueDate.ClientID%>').value = strDueDate;
            $find('popup').hide(); return false;
        }
        function tabvalidate(e) {
            var KeyID = (window.event) ? event.keyCode : e.which;
            if ((KeyID != 9)) {
                return false;
                }
            else{
                return true;
                }
        }
//        function renewal() {
//          if (document.getElementById("<%=txtPass.ClientID%>").value == "") {
//                alert("Please Selects The Duedate Its Empty Now");
//                document.getElementById("<%=txtDueDate.ClientID%>").focus();
//                return false;   
//                }
    </script>
  <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lbtnrenew"
        PopupControlID="Panel1" BehaviorID="popup" BackgroundCssClass="modalBackground"
        PopupDragHandleControlID="Panel1">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="Panel1" Style="display: none" Width="450px" Height="200px" CssClass="modalpopup"
        runat="server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="header">
                   <asp:Label ID="lblRenewalDetails" runat="server" CssClass="UserNameTimeLabel" Text="Renewal Details" />
                    <asp:LinkButton ID="lnkbClose" runat="server" OnClientClick="pass()">
                        <asp:Image ID="Image1" runat="server" CssClass="close" ImageUrl="~/images/close-button.JPG" />
                    </asp:LinkButton>
                </div>
                <div class="container">
                    <asp:GridView ID="gvRenewal" runat="server" Width="450px" Height="175px" AutoGenerateColumns="false"
                        AllowPaging="true" AllowSorting="true" HeaderStyle-BackColor="silver" HeaderStyle-VerticalAlign="top"
                        ShowFooter="True" OnRowCommand="gvRenewal_RowCommand" SkinID="gvCPopUp">
                      <%--OnDataBound="gvRenewal_RowDataBound"--%>
                        <%-- OnRowDeleting="Delete"--%>
                        <%-- OnRowEditing="edit"--%>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="70px" />
                                <ItemTemplate>
                                    <%--<asp:LinkButton ID="lnkbEdit" runat="server" Visible="true" CommandName="Edit" Text="Edit" />--%>
                                    <%-- <asp:LinkButton ID="LinkButton1" runat="server" Visible="true" CommandName="Delete" Text="Delete" />--%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lnkbAdd" runat="server" Visible="true" CommandName="Add" Text="Add New" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="SerialNumber">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle Width="90px" HorizontalAlign="Center"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSerialNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNumber") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Center" />
                                <FooterTemplate>
                                    <asp:Label ID="lblSerialNumber" runat="server" Visible="true">
                                    </asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="RenewalDate">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle Width="170px" HorizontalAlign="Center"></HeaderStyle>
                                <%--<EditItemTemplate>
                                             <asp:TextBox ID="txtRenewal" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RenewalDate") %>' onKeyPress="javascript:return false;" onKeyDown="javascript:return false;"></asp:TextBox>
                                        <asp:ImageButton ID="btnRenewal" runat="server"  
                                        ImageUrl="~/images/Calendar.png" />
                                        <cc1:CalendarExtender ID="CalendarExtender5" runat="server" OnClientShown="onCalendarShown" TargetControlID="txtRenewal" Format="MM/dd/yyyy" PopupButtonID="btnRenewal">
                                         </cc1:CalendarExtender>
                                    </EditItemTemplate>--%>
                                <ItemTemplate>
                                    <asp:Label ID="lblRenewalDate" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container.DataItem, "RenewalDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>                                                          
                                    <asp:TextBox ID="txtRenewalDate" runat="server" onKeyPress="javascript:return false;" onKeyDown="javascript:return false;" SkinID="TxtCal">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="btnRenewalDate" runat="server" ImageUrl="~/images/Calendar.png" />
                                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server"
                                        TargetControlID="txtRenewalDate" Format="MM/dd/yyyy" PopupButtonID="btnRenewalDate">
                                    </cc1:CalendarExtender>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="False" HeaderText="Desc">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TransactionId")%>'
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div style="display: none; visibility: hidden">
                        <asp:TextBox ID="txtPass" Visible="true" runat="server"></asp:TextBox>
                    </div>
                </div> 
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
 </ContentTemplate>
        </asp:UpdatePanel> 
</asp:Content>
