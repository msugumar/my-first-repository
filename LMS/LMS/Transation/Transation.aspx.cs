﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using LMSBLL;
using LMSDAL;
using ExceptionalHandling;
using AjaxControlToolkit;
using System.DirectoryServices;

public partial class Master_issue : System.Web.UI.Page
{
    public String strErrorMessage; 
    public String strPage;
    public String[] strResult;
    public LMSBLL.IssueInfo objInfo;
    public LMSDAL.IssueDAL objDAL;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            txtDueDate.Attributes.Add("onchange", "duedatevalidate()");
            txtIssueDate.Attributes.Add("onKeyPress", "javascript:return false;");
            txtDueDate.Attributes.Add("onKeyPress", "javascript:return false;");
            txtReturnDate.Attributes.Add("onKeyPress", "javascript:return false;");
            txtIssueDate.Attributes.Add("onKeyDown", "return tabvalidate(event)");
            txtDueDate.Attributes.Add("onKeyDown", "return tabvalidate(event)");
            txtReturnDate.Attributes.Add("onKeyDown", "return tabvalidate(event)");
            txtReturnDate.Attributes.Add("onchange", "returndatevalidate()");
            chkReturned.Attributes.Add("onclick", "check()");
            txtIssueDate.Attributes.Add("onchange", "IssueDatevalidate()");
            if (!Page.IsPostBack)
            {
                //txtIssueDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                //txtDueDate.Text = DateTime.Now.AddDays(7).ToString("MM/dd/yyyy");
                AccessIdentifier();
                PopulateIssueDetails();
                PopulateItemTypeDetails();
                PopulateItemDetails();
                PopulateUserDetails();
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            objErrorHandler.RaiseError(ex, strErrorMessage);
        }//if not post back ends
    }

    #region "Populate Transaction"
    private void PopulateIssueDetails()
    {
        try
        {
            objDAL = new LMSDAL.IssueDAL();
            DataView dvIssue = new DataView();
            if ((ViewState["sortExpr"]) != null)
            {
                dvIssue.Table = objDAL.GetIssueInfo().Tables[0];
                dvIssue.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
            }
            else
            {
                dvIssue = objDAL.GetIssueInfo().Tables[0].DefaultView;
            }

            gvIssue.DataSource = dvIssue;
            gvIssue.DataBind();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");                                                                                                                                                                                                              
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    protected void gvIssue_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            gvIssue.PageIndex = e.NewPageIndex;
            PopulateIssueDetails();

        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
    } 
    protected void gvIssue_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            ViewState["sortExpr"] = e.SortExpression;
            if ((String)ViewState["sortDirection"] == "ASC")
            {
                ViewState["sortDirection"] = "DESC";
            }
            else
            {
                ViewState["sortDirection"] = "ASC";
            }
            PopulateIssueDetails();
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }
    #endregion

    #region "Populate ItemType"
    private void PopulateItemTypeDetails()
    {
        try
        {
            objDAL = new LMSDAL.IssueDAL();
            ddlItemType.DataSource = objDAL.GetItemTypeInfo().Tables[0];
            ddlItemType.DataTextField = "ItemType";
            ddlItemType.DataValueField = "ItemTypeId";
            ViewState["ItemTypeId"] = ddlItemType.SelectedValue; 
            ddlItemType.DataBind();
            ddlItemType.Items.Insert(0, new ListItem("--Select Here--", "0"));
            //ddlItemType.Items.Insert(0, "--Select Here--");

        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }


    protected void ddlItemType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack)
            {
                PopulateItemDetails();
                //PopulateFilterIssueDetails();
                txtIssueDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtDueDate.Text = DateTime.Now.AddDays(7).ToString("MM/dd/yyyy");
            }

        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    #endregion

    #region "Populate Item"
    private void PopulateItemDetails()
    {
        try
        {
            objInfo = new LMSBLL.IssueInfo();
            objDAL = new LMSDAL.IssueDAL();
            objInfo.ItemTypeId = Convert.ToInt32(ddlItemType.SelectedValue);
            ddlItem.DataSource = objDAL.GetFilterItemInfo(objInfo);
            //ddlItem.DataSource = objDAL.GetItemInfo().Tables[0];                
            ddlItem.DataTextField = "Title";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            //ddlItem.Items.Insert(0, "--Select Here--");
            ddlItem.Items.Insert(0, new ListItem("--Select Here--", "0"));

        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");

        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    #endregion

    #region "Populate User"
    private void PopulateUserDetails()
    {
        try
        {
            objDAL = new LMSDAL.IssueDAL();
            ddlIssuedTo.DataSource = objDAL.GetUserInfo().Tables[0];
            ddlIssuedTo.DataTextField = "UserName";
            ddlIssuedTo.DataValueField = "UserId";
            ddlIssuedTo.DataBind();
            ddlIssuedTo.Items.Insert(0, new ListItem("--Select Here--", "0"));

        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    #endregion

    #region "Save"
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveIssueDetails();
    }
    private void SaveIssueDetails()
    {
        try
        {
            objInfo = new LMSBLL.IssueInfo();
            objInfo.TransactionId = Convert.ToInt32(ViewState["TransactionId"]);
            objInfo.ItemId = Convert.ToInt32(ddlItem.SelectedValue);
            objInfo.UserId = Convert.ToInt32(ddlIssuedTo.SelectedValue);
            objInfo.IssueDate = Convert.ToDateTime(txtIssueDate.Text);
            objInfo.DueDate = Convert.ToDateTime(txtDueDate.Text);
            if (chkReturned.Checked == true)
            {
                objInfo.ReturnDate = Convert.ToDateTime(txtReturnDate.Text);
                objInfo.StatusId = Convert.ToInt32("1");
                objInfo.ReturnedFlag = Convert.ToInt32("1");
            }
            else
            {
                objInfo.ReturnDate = Convert.ToDateTime("01/01/1900");
                objInfo.StatusId = Convert.ToInt32("2");
                objInfo.ReturnedFlag = Convert.ToInt32("0");
            }
            objDAL = new LMSDAL.IssueDAL();
            int intOutput = objDAL.SaveIssueInfo(objInfo);
            PopulateIssueDetails();
            Label lblErrorMessage = new Label();
            switch (intOutput)
            {
                case 0:
                    lblErrorMessage.Text = "Item Already Issued";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Item Already Issued');</script>", false);
                    Clear();
                    break;
 
                case 1:
                    lblErrorMessage.Text = "Inserted Successfully";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Transaction details Inserted successfully');</script>", false);
                    Clear();
                    break;
                case 2:
                    lblErrorMessage.Text = "Updated Successfully";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('The Transaction details Updated successfully');</script>", false);
                    Clear();
                    break;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }
    #endregion

    #region "Issue RowCommand"
    protected void gvIssue_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                lbtnRenewal.Enabled = true;
                chkReturned.Enabled = true;
                string strItemType = ((System.Web.UI.WebControls.Label)(gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblItemType"))).Text.ToString();
                ddlItemType.SelectedIndex = ddlItemType.Items.IndexOf(ddlItemType.Items.FindByText(strItemType));
                PopulateItemDetails();
                txtIssueDate.Text = ((gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].Cells[3].Text.ToString()));
                ddlItem.SelectedIndex = ddlItem.Items.IndexOf(ddlItem.Items.FindByText(((gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text.ToString()))));
                ddlIssuedTo.SelectedIndex = ddlIssuedTo.Items.IndexOf(ddlIssuedTo.Items.FindByText(((gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text))));
                ViewState["TransactionId"] = ((System.Web.UI.WebControls.Label)(gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblTransactionId"))).Text.ToString();
                txtDueDate.Text = ((System.Web.UI.WebControls.Label)(gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lblDueDate"))).Text.ToString();
                string returndate = ((gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].Cells[5].Text.ToString()));    
                //string status = ((gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].Cells[4].Text.ToString()));
                if (returndate == "&nbsp;")
                //if (status == "Available")
                {
                    txtReturnDate.Text = "";
                    btnSave.Enabled = true;
                    lbtnRenewal.Enabled = true;
                    chkReturned.Checked = false;
                }
                else
                {
                    txtReturnDate.Enabled = true;
                    btnSave.Enabled = false;
                    chkReturned.Checked = true;
                    lbtnRenewal.Enabled = false;
                    txtReturnDate.Text = ((gvIssue.Rows[Convert.ToInt32(e.CommandArgument)].Cells[5].Text.ToString()));
                    
                }

            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
    }
    #endregion 

    #region "Cancel"
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();

    }
    private void Clear()
    {
        //ddlItemType.SelectedIndex = 0;
        ddlItemType.SelectedIndex = ddlItemType.Items.IndexOf(ddlItemType.Items.FindByText("--Select Here--"));
        ddlIssuedTo.SelectedIndex = ddlIssuedTo.Items.IndexOf(ddlIssuedTo.Items.FindByText("--Select Here--"));
        ddlItem.SelectedIndex = ddlItem.Items.IndexOf(ddlItem.Items.FindByText("--Select Here--"));
        txtDueDate.Text = "";
        txtIssueDate.Text = "";
        txtReturnDate.Text = "";
        chkReturned.Enabled = false;
        chkReturned.Checked = false;
        btnSave.Enabled = true;
        lbtnRenewal.Enabled = false;
        ViewState["TransactionId"] = 0;
    }
    #endregion  
    
    #region "Populate Renewal"
    private void PopulateFilterRenewalDetails()
    {
        try
        {
            objInfo = new LMSBLL.IssueInfo();
            objDAL = new LMSDAL.IssueDAL();
            objInfo.TransactionId = Convert.ToInt32(ViewState["TransactionId"]);
            if (objDAL.GetFilterRenewalInfo(objInfo).Tables[0].Rows.Count == 0)
            {
                DataView dv = new DataView(objDAL.GetFilterRenewalInfo(objInfo).Tables[0]);
                DataRowView newrow = dv.AddNew();               
                    //dv.Table = objDAL.GetFilterRenewalInfo(objInfo).Tables[0];
                    //DataTable dt = new DataTable(dv.Table.TableName);
                    //copy columns
                    //foreach (DataColumn dc in dv.Table.Columns)
                    //{
                    //    dt.Columns.Add(dc.ColumnName);
                    //}
                    //Add a row and use that new view.
                    //dt.Rows.Add(dt.NewRow());
                    //dv = new DataView(dt);
                //objDAL.GetFilterRenewalInfo(objInfo).Tables[0].Rows.Add(objDAL.GetFilterRenewalInfo(objInfo).Tables[0].NewRow());
                gvRenewal.DataSource =dv;
                gvRenewal.DataBind();
                //int columnCount = gvRenewal.Rows[0].Cells.Count;
                //gvRenewal.Rows[0].Cells.Clear();
                //gvRenewal.Rows[0].Cells.Add(new TableCell());
                //gvRenewal.Rows[0].Cells[0].ColumnSpan = 3;
                //gvRenewal.Rows[0].Cells[0].Text = "No Records Found.";              

            }
            else
            {
                //objInfo.TransactionId = Convert.ToInt32(ViewState["TransactionId"]);
                gvRenewal.DataSource = objDAL.GetFilterRenewalInfo(objInfo).Tables[0];
                gvRenewal.DataBind();

            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;          
        }
    }
    #endregion \\

    #region "Renewal RowCommand"
    protected void gvRenewal_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
        if (e.CommandName == "Add")
        {
            try
            {
                objInfo = new LMSBLL.IssueInfo();
                objInfo.RenewalId = Convert.ToInt32(ViewState["RenewalId"]);
                objInfo.TransactionId = Convert.ToInt32(ViewState["TransactionId"]);
                TextBox txtRenewalDate = ((TextBox)(gvRenewal.FooterRow.FindControl("txtRenewalDate")));
                //LinkButton lbtnRenewalDate = ((LinkButton)(gvRenewal.FooterRow.FindControl("lnkbAdd")));
                if (txtRenewalDate.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Select the Renewal Date');</script>", false);
                }
                else
                {
                    objInfo.RenewalDate = Convert.ToDateTime(txtRenewalDate.Text);

                    if (Convert.ToDateTime(txtRenewalDate.Text) > Convert.ToDateTime(txtDueDate.Text))
                    {
                        if (Convert.ToDateTime(txtRenewalDate.Text) > Convert.ToDateTime(txtIssueDate.Text))
                        {
                            if (gvRenewal.Rows.Count < 3)
                            {
                                objDAL = new LMSDAL.IssueDAL();
                                int intOutput = objDAL.SaveRenewalInfo(objInfo);
                                PopulateFilterRenewalDetails();
                                ModalPopupExtender1.Show();
                                Label lblErrorMessage = new Label();
                                switch (intOutput)
                                {
                                    case 0:
                                        lblErrorMessage.Text = "Please Select The Transaction For Renewal";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Error Occured');</script>", false);
                                        break;

                                    case 1:
                                        lblErrorMessage.Text = "Inserted Successfully";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Renewal details Inserted successfully');</script>", false);
                                        break;

                                    case 2:
                                        lblErrorMessage.Text = "Inserted Successfully";
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Renewal details Inserted successfully');</script>", false);
                                        break;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Renewal only for three times');</script>", false);
                            }
                            txtPass.Text = txtRenewalDate.Text;
                            //lbtnRenewalDate.Attributes.Add("onclick", "renewal()");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Ensure That Renewal Date is Greater Than Issue Date');</script>", false);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Msg", "<script>alert('Please Ensure That Renewal Date is Greater Than Due Date');</script>", false);
                    }
                }
            }

            catch (Exception ex)
            {
                ErrorHandler objErrorHandler = new ErrorHandler();
                int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
                Session["ErrorID"] = intError.ToString();
                Response.Redirect("CustomError.aspx");
                //strErrorMessage = ex.Message;

            }
            finally
            {
                objInfo = null;
                objDAL = null;
            }
            PopulateIssueDetails();
        }
    }
    #endregion

    #region "Renewal"
    protected void lbtnRenewal_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.TargetControlID = "lbtnRenewal";
        ModalPopupExtender1.Show();
        PopulateFilterRenewalDetails();  
     
    }
    #endregion

    //protected void edit(object sender, GridViewEditEventArgs e)
    //{
    //    gvRenewal.EditIndex = e.NewEditIndex;
    //      PopulateFilterRenewalDetails();
    //}
    //protected void Delete(object sender, GridViewDeleteEventArgs e)
    //{
    //    int ID = (int)gvRenewal.DataKeys[e.RowIndex].Value;
  
    //      objInfo = new LMSBLL.IssueInfo();
    //      objDAL = new LMSDAL.IssueDAL();
    //      objInfo.TransactionId = Convert.ToInt32(ViewState["TransactionId"]);

    //}

    private void AccessIdentifier()
    {
        try
        {
            objInfo = new LMSBLL.IssueInfo();
            objDAL = new LMSDAL.IssueDAL();
            //objDAL = new LMSDAL.ItemMaintainDal();
            strPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
            WindowsPrincipal struser = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            //objInfo.LoginId = ExtractUserName(struser.Identity.Name);      
            objInfo.LoginId = ExtractUserName(struser.Identity.Name);

            int intLen = 0;
            intLen = strPage.IndexOf(".");
            objInfo.FormName = strPage.Substring(0, intLen);
            strResult = objDAL.Accessverifyer(objInfo);

            if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == true)
            {
                btnSave.Enabled = true;
            }
            else if ((Convert.ToBoolean(strResult[0])) == true & (Convert.ToBoolean(strResult[1])) == false)
            {
                btnSave.Enabled = false;
            }
            else
            {
                AccordionPane AccordionPaneItemMaintain = (AccordionPane)Master.FindControl("AccordionPane2");
                LinkButton lnkItemMaintain = (LinkButton)AccordionPaneItemMaintain.FindControl("lnkTransaction");
                Response.Redirect("~/UnAuthorization.aspx?file=" + lnkItemMaintain.Text.ToString(), false);
                //return;
            }
        }
        catch (Exception ex)
        {
            ErrorHandler objErrorHandler = new ErrorHandler();
            int intError = objErrorHandler.RaiseError(ex, strErrorMessage);
            Session["ErrorID"] = intError.ToString();
            Response.Redirect("CustomError.aspx");
        }
        finally
        {
            objInfo = null;
            objDAL = null;
        }
    }

    public string ExtractUserName(string path)
    {
        /*  try
          {*/
        string[] userPath = path.Split(new char[] { '\\' });

        return userPath[userPath.Length - 1];
        /*      }
              catch (Exception ex)
         * 
              {
                  ErrorHandler.RaiseError(ex, strLogError);
              } */
    }

    //#region "Populate Transac"
    //private void PopulateFilterIssueDetails()
    //{
    //    try
    //    {
    //        objInfo = new LMSBLL.IssueInfo();           
    //        objInfo.ItemTypeId = Convert.ToInt32(ddlItemType.SelectedValue);
    //        objDAL = new LMSDAL.IssueDAL();
    //        DataView dvIssue = new DataView();
    //        if ((ViewState["sortExpr"]) != null)
    //        {
    //            dvIssue.Table = objDAL.GetFilterIssueInfo(objInfo).Tables[0];
    //            dvIssue.Sort = (string)ViewState["sortExpr"] + " " + ViewState["sortDirection"];
    //        }
    //        else
    //        {
    //            dvIssue = objDAL.GetFilterIssueInfo(objInfo).Tables[0].DefaultView;
    //        }

    //        gvIssue.DataSource = dvIssue;
    //        gvIssue.DataBind();
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler objErrorHandler = new ErrorHandler();
    //        objErrorHandler.RaiseError(ex, strErrorMessage);
    //        strErrorMessage = ex.Message;
    //    }
    //    finally
    //    {
    //        objInfo = null;
    //        objDAL = null;
    //    }
    //}
    ////protected void gvIssue_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    ////{
    ////    try
    ////    {
    ////        gvIssue.PageIndex = e.NewPageIndex;
    ////        PopulateFilterIssueDetails();

    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        ErrorHandler objErrorHandler = new ErrorHandler();
    ////        objErrorHandler.RaiseError(ex, strErrorMessage);
    ////        strErrorMessage = ex.Message;

    ////    }
    ////}
    ////protected void gvIssue_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    ////{
    ////    try
    ////    {
    ////        ViewState["sortExpr"] = e.SortExpression;
    ////        if ((String)ViewState["sortDirection"] == "ASC")
    ////        {
    ////            ViewState["sortDirection"] = "DESC";
    ////        }
    ////        else
    ////        {
    ////            ViewState["sortDirection"] = "ASC";
    ////        }
    ////        PopulateFilterIssueDetails();
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        ErrorHandler objErrorHandler = new ErrorHandler();
    ////        objErrorHandler.RaiseError(ex, strErrorMessage);
    ////        strErrorMessage = ex.Message;

    ////    }
    ////}
    //#endregion
    //protected void gvRenewal_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.Footer)
    //    {
    //        //Find the checkbox control in header and add an attribute
    //        ((TextBox)e.Row.FindControl("txtRenewalDate")).Attributes.Add("onchange", "javascript:SelectAll('" +
    //                ((TextBox)e.Row.FindControl("txtRenewalDate")).ClientID + "')");
    //    }
    //}
}






              
   

