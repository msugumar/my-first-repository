﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using UserAuthentication;
using System.DirectoryServices;
using System.Security;



public partial class LMSMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string domainUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        //String userFullName = UserPrincipal.Current.DisplayName;
        string[] paramsLogin = domainUser.Split('\\');
        lblUserName.Text = paramsLogin[1].ToString();
        lblDateTime.Text = DateTime.Now.ToString("MMMM dd,[dddd]");

        System.Security.Principal.WindowsIdentity wi = System.Security.Principal.WindowsIdentity.GetCurrent();
        string[] a = Context.User.Identity.Name.Split('\\');
        
        
        System.DirectoryServices.DirectoryEntry ADEntry = new System.DirectoryServices.DirectoryEntry("WinNT://" + a[0] + "/" + a[1]);
        string Name = ADEntry.Properties["FullName"].Value.ToString();


    }
    protected void lnkRolePrivilage_Click(object sender, EventArgs e)
    {
    }
    protected void lnkBookMaintain_Click(object sender, EventArgs e)
    {
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
    }
    protected void ImgLogo_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/LMSHome.aspx");
    }
}
